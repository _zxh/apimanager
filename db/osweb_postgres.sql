/*
PGSQL Backup
Source Server Version: 9.4.9
Source Database: kong
Date: 2016/9/9 13:59:12
*/


-- ----------------------------
--  Sequence definition for "public"."act_evt_log_log_nr__seq"
-- ----------------------------
DROP SEQUENCE "public"."act_evt_log_log_nr__seq";
CREATE SEQUENCE "public"."act_evt_log_log_nr__seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;;

-- ----------------------------
--  Table structure for "public"."act_evt_log"
-- ----------------------------
DROP TABLE "public"."act_evt_log";
CREATE TABLE "public"."act_evt_log" (
"log_nr_" int4 DEFAULT nextval('act_evt_log_log_nr__seq'::regclass) NOT NULL,
"type_" varchar(64) COLLATE "default",
"proc_def_id_" varchar(64) COLLATE "default",
"proc_inst_id_" varchar(64) COLLATE "default",
"execution_id_" varchar(64) COLLATE "default",
"task_id_" varchar(64) COLLATE "default",
"time_stamp_" timestamp(6) NOT NULL,
"user_id_" varchar(255) COLLATE "default",
"data_" bytea,
"lock_owner_" varchar(255) COLLATE "default",
"lock_time_" timestamp(6),
"is_processed_" int2 DEFAULT 0,
PRIMARY KEY ("log_nr_")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_ge_bytearray"
-- ----------------------------
DROP TABLE "public"."act_ge_bytearray";
CREATE TABLE "public"."act_ge_bytearray" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"name_" varchar(255) COLLATE "default",
"deployment_id_" varchar(64) COLLATE "default",
"bytes_" bytea,
"generated_" bool,
PRIMARY KEY ("id_"),
FOREIGN KEY ("deployment_id_") REFERENCES "public"."act_re_deployment" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_ge_property"
-- ----------------------------
DROP TABLE "public"."act_ge_property";
CREATE TABLE "public"."act_ge_property" (
"name_" varchar(64) COLLATE "default" NOT NULL,
"value_" varchar(300) COLLATE "default",
"rev_" int4,
PRIMARY KEY ("name_")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_hi_actinst"
-- ----------------------------
DROP TABLE "public"."act_hi_actinst";
CREATE TABLE "public"."act_hi_actinst" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"proc_def_id_" varchar(64) COLLATE "default" NOT NULL,
"proc_inst_id_" varchar(64) COLLATE "default" NOT NULL,
"execution_id_" varchar(64) COLLATE "default" NOT NULL,
"act_id_" varchar(255) COLLATE "default" NOT NULL,
"task_id_" varchar(64) COLLATE "default",
"call_proc_inst_id_" varchar(64) COLLATE "default",
"act_name_" varchar(255) COLLATE "default",
"act_type_" varchar(255) COLLATE "default" NOT NULL,
"assignee_" varchar(255) COLLATE "default",
"start_time_" timestamp(6) NOT NULL,
"end_time_" timestamp(6),
"duration_" int8,
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_hi_attachment"
-- ----------------------------
DROP TABLE "public"."act_hi_attachment";
CREATE TABLE "public"."act_hi_attachment" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"user_id_" varchar(255) COLLATE "default",
"name_" varchar(255) COLLATE "default",
"description_" varchar(4000) COLLATE "default",
"type_" varchar(255) COLLATE "default",
"task_id_" varchar(64) COLLATE "default",
"proc_inst_id_" varchar(64) COLLATE "default",
"url_" varchar(4000) COLLATE "default",
"content_id_" varchar(64) COLLATE "default",
"time_" timestamp(6),
PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_hi_comment"
-- ----------------------------
DROP TABLE "public"."act_hi_comment";
CREATE TABLE "public"."act_hi_comment" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"type_" varchar(255) COLLATE "default",
"time_" timestamp(6) NOT NULL,
"user_id_" varchar(255) COLLATE "default",
"task_id_" varchar(64) COLLATE "default",
"proc_inst_id_" varchar(64) COLLATE "default",
"action_" varchar(255) COLLATE "default",
"message_" varchar(4000) COLLATE "default",
"full_msg_" bytea,
PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_hi_detail"
-- ----------------------------
DROP TABLE "public"."act_hi_detail";
CREATE TABLE "public"."act_hi_detail" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"type_" varchar(255) COLLATE "default" NOT NULL,
"proc_inst_id_" varchar(64) COLLATE "default",
"execution_id_" varchar(64) COLLATE "default",
"task_id_" varchar(64) COLLATE "default",
"act_inst_id_" varchar(64) COLLATE "default",
"name_" varchar(255) COLLATE "default" NOT NULL,
"var_type_" varchar(64) COLLATE "default",
"rev_" int4,
"time_" timestamp(6) NOT NULL,
"bytearray_id_" varchar(64) COLLATE "default",
"double_" float8,
"long_" int8,
"text_" varchar(4000) COLLATE "default",
"text2_" varchar(4000) COLLATE "default",
PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_hi_identitylink"
-- ----------------------------
DROP TABLE "public"."act_hi_identitylink";
CREATE TABLE "public"."act_hi_identitylink" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"group_id_" varchar(255) COLLATE "default",
"type_" varchar(255) COLLATE "default",
"user_id_" varchar(255) COLLATE "default",
"task_id_" varchar(64) COLLATE "default",
"proc_inst_id_" varchar(64) COLLATE "default",
PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_hi_procinst"
-- ----------------------------
DROP TABLE "public"."act_hi_procinst";
CREATE TABLE "public"."act_hi_procinst" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"proc_inst_id_" varchar(64) COLLATE "default" NOT NULL,
"business_key_" varchar(255) COLLATE "default",
"proc_def_id_" varchar(64) COLLATE "default" NOT NULL,
"start_time_" timestamp(6) NOT NULL,
"end_time_" timestamp(6),
"duration_" int8,
"start_user_id_" varchar(255) COLLATE "default",
"start_act_id_" varchar(255) COLLATE "default",
"end_act_id_" varchar(255) COLLATE "default",
"super_process_instance_id_" varchar(64) COLLATE "default",
"delete_reason_" varchar(4000) COLLATE "default",
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"name_" varchar(255) COLLATE "default",
PRIMARY KEY ("id_"),
UNIQUE ("proc_inst_id_")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_hi_taskinst"
-- ----------------------------
DROP TABLE "public"."act_hi_taskinst";
CREATE TABLE "public"."act_hi_taskinst" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"proc_def_id_" varchar(64) COLLATE "default",
"task_def_key_" varchar(255) COLLATE "default",
"proc_inst_id_" varchar(64) COLLATE "default",
"execution_id_" varchar(64) COLLATE "default",
"name_" varchar(255) COLLATE "default",
"parent_task_id_" varchar(64) COLLATE "default",
"description_" varchar(4000) COLLATE "default",
"owner_" varchar(255) COLLATE "default",
"assignee_" varchar(255) COLLATE "default",
"start_time_" timestamp(6) NOT NULL,
"claim_time_" timestamp(6),
"end_time_" timestamp(6),
"duration_" int8,
"delete_reason_" varchar(4000) COLLATE "default",
"priority_" int4,
"due_date_" timestamp(6),
"form_key_" varchar(255) COLLATE "default",
"category_" varchar(255) COLLATE "default",
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_hi_varinst"
-- ----------------------------
DROP TABLE "public"."act_hi_varinst";
CREATE TABLE "public"."act_hi_varinst" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"proc_inst_id_" varchar(64) COLLATE "default",
"execution_id_" varchar(64) COLLATE "default",
"task_id_" varchar(64) COLLATE "default",
"name_" varchar(255) COLLATE "default" NOT NULL,
"var_type_" varchar(100) COLLATE "default",
"rev_" int4,
"bytearray_id_" varchar(64) COLLATE "default",
"double_" float8,
"long_" int8,
"text_" varchar(4000) COLLATE "default",
"text2_" varchar(4000) COLLATE "default",
"create_time_" timestamp(6),
"last_updated_time_" timestamp(6),
PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_id_group"
-- ----------------------------
DROP TABLE "public"."act_id_group";
CREATE TABLE "public"."act_id_group" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"name_" varchar(255) COLLATE "default",
"type_" varchar(255) COLLATE "default",
PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_id_info"
-- ----------------------------
DROP TABLE "public"."act_id_info";
CREATE TABLE "public"."act_id_info" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"user_id_" varchar(64) COLLATE "default",
"type_" varchar(64) COLLATE "default",
"key_" varchar(255) COLLATE "default",
"value_" varchar(255) COLLATE "default",
"password_" bytea,
"parent_id_" varchar(255) COLLATE "default",
PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_id_membership"
-- ----------------------------
DROP TABLE "public"."act_id_membership";
CREATE TABLE "public"."act_id_membership" (
"user_id_" varchar(64) COLLATE "default" NOT NULL,
"group_id_" varchar(64) COLLATE "default" NOT NULL,
PRIMARY KEY ("user_id_", "group_id_"),
FOREIGN KEY ("group_id_") REFERENCES "public"."act_id_group" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION,
FOREIGN KEY ("user_id_") REFERENCES "public"."act_id_user" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_id_user"
-- ----------------------------
DROP TABLE "public"."act_id_user";
CREATE TABLE "public"."act_id_user" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"first_" varchar(255) COLLATE "default",
"last_" varchar(255) COLLATE "default",
"email_" varchar(255) COLLATE "default",
"pwd_" varchar(255) COLLATE "default",
"picture_id_" varchar(64) COLLATE "default",
PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_procdef_info"
-- ----------------------------
DROP TABLE "public"."act_procdef_info";
CREATE TABLE "public"."act_procdef_info" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"proc_def_id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"info_json_id_" varchar(64) COLLATE "default",
PRIMARY KEY ("id_"),
FOREIGN KEY ("proc_def_id_") REFERENCES "public"."act_re_procdef" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION,
FOREIGN KEY ("info_json_id_") REFERENCES "public"."act_ge_bytearray" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION,
UNIQUE ("proc_def_id_")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_re_deployment"
-- ----------------------------
DROP TABLE "public"."act_re_deployment";
CREATE TABLE "public"."act_re_deployment" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"name_" varchar(255) COLLATE "default",
"category_" varchar(255) COLLATE "default",
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"deploy_time_" timestamp(6),
PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_re_model"
-- ----------------------------
DROP TABLE "public"."act_re_model";
CREATE TABLE "public"."act_re_model" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"name_" varchar(255) COLLATE "default",
"key_" varchar(255) COLLATE "default",
"category_" varchar(255) COLLATE "default",
"create_time_" timestamp(6),
"last_update_time_" timestamp(6),
"version_" int4,
"meta_info_" varchar(4000) COLLATE "default",
"deployment_id_" varchar(64) COLLATE "default",
"editor_source_value_id_" varchar(64) COLLATE "default",
"editor_source_extra_value_id_" varchar(64) COLLATE "default",
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
PRIMARY KEY ("id_"),
FOREIGN KEY ("editor_source_extra_value_id_") REFERENCES "public"."act_ge_bytearray" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION,
FOREIGN KEY ("editor_source_value_id_") REFERENCES "public"."act_ge_bytearray" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION,
FOREIGN KEY ("deployment_id_") REFERENCES "public"."act_re_deployment" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_re_procdef"
-- ----------------------------
DROP TABLE "public"."act_re_procdef";
CREATE TABLE "public"."act_re_procdef" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"category_" varchar(255) COLLATE "default",
"name_" varchar(255) COLLATE "default",
"key_" varchar(255) COLLATE "default" NOT NULL,
"version_" int4 NOT NULL,
"deployment_id_" varchar(64) COLLATE "default",
"resource_name_" varchar(4000) COLLATE "default",
"dgrm_resource_name_" varchar(4000) COLLATE "default",
"description_" varchar(4000) COLLATE "default",
"has_start_form_key_" bool,
"has_graphical_notation_" bool,
"suspension_state_" int4,
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
PRIMARY KEY ("id_"),
UNIQUE ("key_", "version_", "tenant_id_")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_ru_event_subscr"
-- ----------------------------
DROP TABLE "public"."act_ru_event_subscr";
CREATE TABLE "public"."act_ru_event_subscr" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"event_type_" varchar(255) COLLATE "default" NOT NULL,
"event_name_" varchar(255) COLLATE "default",
"execution_id_" varchar(64) COLLATE "default",
"proc_inst_id_" varchar(64) COLLATE "default",
"activity_id_" varchar(64) COLLATE "default",
"configuration_" varchar(255) COLLATE "default",
"created_" timestamp(6) NOT NULL,
"proc_def_id_" varchar(64) COLLATE "default",
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
PRIMARY KEY ("id_"),
FOREIGN KEY ("execution_id_") REFERENCES "public"."act_ru_execution" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_ru_execution"
-- ----------------------------
DROP TABLE "public"."act_ru_execution";
CREATE TABLE "public"."act_ru_execution" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"proc_inst_id_" varchar(64) COLLATE "default",
"business_key_" varchar(255) COLLATE "default",
"parent_id_" varchar(64) COLLATE "default",
"proc_def_id_" varchar(64) COLLATE "default",
"super_exec_" varchar(64) COLLATE "default",
"act_id_" varchar(255) COLLATE "default",
"is_active_" bool,
"is_concurrent_" bool,
"is_scope_" bool,
"is_event_scope_" bool,
"suspension_state_" int4,
"cached_ent_state_" int4,
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"name_" varchar(255) COLLATE "default",
"lock_time_" timestamp(6),
PRIMARY KEY ("id_"),
FOREIGN KEY ("super_exec_") REFERENCES "public"."act_ru_execution" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION,
FOREIGN KEY ("proc_inst_id_") REFERENCES "public"."act_ru_execution" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION,
FOREIGN KEY ("parent_id_") REFERENCES "public"."act_ru_execution" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION,
FOREIGN KEY ("proc_def_id_") REFERENCES "public"."act_re_procdef" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_ru_identitylink"
-- ----------------------------
DROP TABLE "public"."act_ru_identitylink";
CREATE TABLE "public"."act_ru_identitylink" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"group_id_" varchar(255) COLLATE "default",
"type_" varchar(255) COLLATE "default",
"user_id_" varchar(255) COLLATE "default",
"task_id_" varchar(64) COLLATE "default",
"proc_inst_id_" varchar(64) COLLATE "default",
"proc_def_id_" varchar(64) COLLATE "default",
PRIMARY KEY ("id_"),
FOREIGN KEY ("proc_inst_id_") REFERENCES "public"."act_ru_execution" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION,
FOREIGN KEY ("proc_def_id_") REFERENCES "public"."act_re_procdef" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION,
FOREIGN KEY ("task_id_") REFERENCES "public"."act_ru_task" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_ru_job"
-- ----------------------------
DROP TABLE "public"."act_ru_job";
CREATE TABLE "public"."act_ru_job" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"type_" varchar(255) COLLATE "default" NOT NULL,
"lock_exp_time_" timestamp(6),
"lock_owner_" varchar(255) COLLATE "default",
"exclusive_" bool,
"execution_id_" varchar(64) COLLATE "default",
"process_instance_id_" varchar(64) COLLATE "default",
"proc_def_id_" varchar(64) COLLATE "default",
"retries_" int4,
"exception_stack_id_" varchar(64) COLLATE "default",
"exception_msg_" varchar(4000) COLLATE "default",
"duedate_" timestamp(6),
"repeat_" varchar(255) COLLATE "default",
"handler_type_" varchar(255) COLLATE "default",
"handler_cfg_" varchar(4000) COLLATE "default",
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
PRIMARY KEY ("id_"),
FOREIGN KEY ("exception_stack_id_") REFERENCES "public"."act_ge_bytearray" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_ru_task"
-- ----------------------------
DROP TABLE "public"."act_ru_task";
CREATE TABLE "public"."act_ru_task" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"execution_id_" varchar(64) COLLATE "default",
"proc_inst_id_" varchar(64) COLLATE "default",
"proc_def_id_" varchar(64) COLLATE "default",
"name_" varchar(255) COLLATE "default",
"parent_task_id_" varchar(64) COLLATE "default",
"description_" varchar(4000) COLLATE "default",
"task_def_key_" varchar(255) COLLATE "default",
"owner_" varchar(255) COLLATE "default",
"assignee_" varchar(255) COLLATE "default",
"delegation_" varchar(64) COLLATE "default",
"priority_" int4,
"create_time_" timestamp(6),
"due_date_" timestamp(6),
"category_" varchar(255) COLLATE "default",
"suspension_state_" int4,
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"form_key_" varchar(255) COLLATE "default",
PRIMARY KEY ("id_"),
FOREIGN KEY ("execution_id_") REFERENCES "public"."act_ru_execution" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION,
FOREIGN KEY ("proc_def_id_") REFERENCES "public"."act_re_procdef" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION,
FOREIGN KEY ("proc_inst_id_") REFERENCES "public"."act_ru_execution" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."act_ru_variable"
-- ----------------------------
DROP TABLE "public"."act_ru_variable";
CREATE TABLE "public"."act_ru_variable" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"type_" varchar(255) COLLATE "default" NOT NULL,
"name_" varchar(255) COLLATE "default" NOT NULL,
"execution_id_" varchar(64) COLLATE "default",
"proc_inst_id_" varchar(64) COLLATE "default",
"task_id_" varchar(64) COLLATE "default",
"bytearray_id_" varchar(64) COLLATE "default",
"double_" float8,
"long_" int8,
"text_" varchar(4000) COLLATE "default",
"text2_" varchar(4000) COLLATE "default",
PRIMARY KEY ("id_"),
FOREIGN KEY ("bytearray_id_") REFERENCES "public"."act_ge_bytearray" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION,
FOREIGN KEY ("proc_inst_id_") REFERENCES "public"."act_ru_execution" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION,
FOREIGN KEY ("execution_id_") REFERENCES "public"."act_ru_execution" ("id_") ON DELETE NO ACTION ON UPDATE NO ACTION
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."cms_article"
-- ----------------------------
DROP TABLE "public"."cms_article";
CREATE TABLE "public"."cms_article" (
"id" varchar(64) COLLATE "default" NOT NULL,
"category_id" varchar(64) COLLATE "default" NOT NULL,
"title" varchar(255) COLLATE "default" NOT NULL,
"link" varchar(255) COLLATE "default",
"color" varchar(50) COLLATE "default",
"image" varchar(255) COLLATE "default",
"keywords" varchar(255) COLLATE "default",
"description" varchar(255) COLLATE "default",
"weight" int4,
"weight_date" timestamp(6),
"hits" int4,
"posid" varchar(10) COLLATE "default",
"custom_content_view" varchar(255) COLLATE "default",
"view_config" text COLLATE "default",
"create_by" varchar(64) COLLATE "default",
"create_date" timestamp(6),
"update_by" varchar(64) COLLATE "default",
"update_date" timestamp(6),
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."cms_article" IS '文章表';

COMMENT ON COLUMN "public"."cms_article"."id" IS '编号';

COMMENT ON COLUMN "public"."cms_article"."category_id" IS '栏目编号';

COMMENT ON COLUMN "public"."cms_article"."title" IS '标题';

COMMENT ON COLUMN "public"."cms_article"."link" IS '文章链接';

COMMENT ON COLUMN "public"."cms_article"."color" IS '标题颜色';

COMMENT ON COLUMN "public"."cms_article"."image" IS '文章图片';

COMMENT ON COLUMN "public"."cms_article"."keywords" IS '关键字';

COMMENT ON COLUMN "public"."cms_article"."description" IS '描述、摘要';

COMMENT ON COLUMN "public"."cms_article"."weight" IS '权重，越大越靠前';

COMMENT ON COLUMN "public"."cms_article"."weight_date" IS '权重期限';

COMMENT ON COLUMN "public"."cms_article"."hits" IS '点击数';

COMMENT ON COLUMN "public"."cms_article"."posid" IS '推荐位，多选';

COMMENT ON COLUMN "public"."cms_article"."custom_content_view" IS '自定义内容视图';

COMMENT ON COLUMN "public"."cms_article"."view_config" IS '视图配置';

COMMENT ON COLUMN "public"."cms_article"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."cms_article"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."cms_article"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."cms_article"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."cms_article"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."cms_article"."del_flag" IS '删除标记';;

-- ----------------------------
--  Table structure for "public"."cms_article_data"
-- ----------------------------
DROP TABLE "public"."cms_article_data";
CREATE TABLE "public"."cms_article_data" (
"id" varchar(64) COLLATE "default" NOT NULL,
"content" text COLLATE "default",
"copyfrom" varchar(255) COLLATE "default",
"relation" varchar(255) COLLATE "default",
"allow_comment" char(1) COLLATE "default"
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."cms_article_data" IS '文章详表';

COMMENT ON COLUMN "public"."cms_article_data"."id" IS '编号';

COMMENT ON COLUMN "public"."cms_article_data"."content" IS '文章内容';

COMMENT ON COLUMN "public"."cms_article_data"."copyfrom" IS '文章来源';

COMMENT ON COLUMN "public"."cms_article_data"."relation" IS '相关文章';

COMMENT ON COLUMN "public"."cms_article_data"."allow_comment" IS '是否允许评论';;

-- ----------------------------
--  Table structure for "public"."cms_category"
-- ----------------------------
DROP TABLE "public"."cms_category";
CREATE TABLE "public"."cms_category" (
"id" varchar(64) COLLATE "default" NOT NULL,
"parent_id" varchar(64) COLLATE "default" NOT NULL,
"parent_ids" varchar(2000) COLLATE "default" NOT NULL,
"site_id" varchar(64) COLLATE "default",
"office_id" varchar(64) COLLATE "default",
"module" varchar(20) COLLATE "default",
"name" varchar(100) COLLATE "default" NOT NULL,
"image" varchar(255) COLLATE "default",
"href" varchar(255) COLLATE "default",
"target" varchar(20) COLLATE "default",
"description" varchar(255) COLLATE "default",
"keywords" varchar(255) COLLATE "default",
"sort" int4,
"in_menu" char(1) COLLATE "default",
"in_list" char(1) COLLATE "default",
"show_modes" char(1) COLLATE "default",
"allow_comment" char(1) COLLATE "default",
"is_audit" char(1) COLLATE "default",
"custom_list_view" varchar(255) COLLATE "default",
"custom_content_view" varchar(255) COLLATE "default",
"view_config" text COLLATE "default",
"create_by" varchar(64) COLLATE "default",
"create_date" timestamp(6),
"update_by" varchar(64) COLLATE "default",
"update_date" timestamp(6),
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."cms_category" IS '栏目表';

COMMENT ON COLUMN "public"."cms_category"."id" IS '编号';

COMMENT ON COLUMN "public"."cms_category"."parent_id" IS '父级编号';

COMMENT ON COLUMN "public"."cms_category"."parent_ids" IS '所有父级编号';

COMMENT ON COLUMN "public"."cms_category"."site_id" IS '站点编号';

COMMENT ON COLUMN "public"."cms_category"."office_id" IS '归属机构';

COMMENT ON COLUMN "public"."cms_category"."module" IS '栏目模块';

COMMENT ON COLUMN "public"."cms_category"."name" IS '栏目名称';

COMMENT ON COLUMN "public"."cms_category"."image" IS '栏目图片';

COMMENT ON COLUMN "public"."cms_category"."href" IS '链接';

COMMENT ON COLUMN "public"."cms_category"."target" IS '目标';

COMMENT ON COLUMN "public"."cms_category"."description" IS '描述';

COMMENT ON COLUMN "public"."cms_category"."keywords" IS '关键字';

COMMENT ON COLUMN "public"."cms_category"."sort" IS '排序（升序）';

COMMENT ON COLUMN "public"."cms_category"."in_menu" IS '是否在导航中显示';

COMMENT ON COLUMN "public"."cms_category"."in_list" IS '是否在分类页中显示列表';

COMMENT ON COLUMN "public"."cms_category"."show_modes" IS '展现方式';

COMMENT ON COLUMN "public"."cms_category"."allow_comment" IS '是否允许评论';

COMMENT ON COLUMN "public"."cms_category"."is_audit" IS '是否需要审核';

COMMENT ON COLUMN "public"."cms_category"."custom_list_view" IS '自定义列表视图';

COMMENT ON COLUMN "public"."cms_category"."custom_content_view" IS '自定义内容视图';

COMMENT ON COLUMN "public"."cms_category"."view_config" IS '视图配置';

COMMENT ON COLUMN "public"."cms_category"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."cms_category"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."cms_category"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."cms_category"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."cms_category"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."cms_category"."del_flag" IS '删除标记';;

-- ----------------------------
--  Table structure for "public"."cms_comment"
-- ----------------------------
DROP TABLE "public"."cms_comment";
CREATE TABLE "public"."cms_comment" (
"id" varchar(64) COLLATE "default" NOT NULL,
"category_id" varchar(64) COLLATE "default" NOT NULL,
"content_id" varchar(64) COLLATE "default" NOT NULL,
"title" varchar(255) COLLATE "default",
"content" varchar(255) COLLATE "default",
"name" varchar(100) COLLATE "default",
"ip" varchar(100) COLLATE "default",
"create_date" timestamp(6) NOT NULL,
"audit_user_id" varchar(64) COLLATE "default",
"audit_date" timestamp(6),
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."cms_comment" IS '评论表';

COMMENT ON COLUMN "public"."cms_comment"."id" IS '编号';

COMMENT ON COLUMN "public"."cms_comment"."category_id" IS '栏目编号';

COMMENT ON COLUMN "public"."cms_comment"."content_id" IS '栏目内容的编号';

COMMENT ON COLUMN "public"."cms_comment"."title" IS '栏目内容的标题';

COMMENT ON COLUMN "public"."cms_comment"."content" IS '评论内容';

COMMENT ON COLUMN "public"."cms_comment"."name" IS '评论姓名';

COMMENT ON COLUMN "public"."cms_comment"."ip" IS '评论IP';

COMMENT ON COLUMN "public"."cms_comment"."create_date" IS '评论时间';

COMMENT ON COLUMN "public"."cms_comment"."audit_user_id" IS '审核人';

COMMENT ON COLUMN "public"."cms_comment"."audit_date" IS '审核时间';

COMMENT ON COLUMN "public"."cms_comment"."del_flag" IS '删除标记';;

-- ----------------------------
--  Table structure for "public"."cms_guestbook"
-- ----------------------------
DROP TABLE "public"."cms_guestbook";
CREATE TABLE "public"."cms_guestbook" (
"id" varchar(64) COLLATE "default" NOT NULL,
"type" char(1) COLLATE "default" NOT NULL,
"content" varchar(255) COLLATE "default" NOT NULL,
"name" varchar(100) COLLATE "default" NOT NULL,
"email" varchar(100) COLLATE "default" NOT NULL,
"phone" varchar(100) COLLATE "default" NOT NULL,
"workunit" varchar(100) COLLATE "default" NOT NULL,
"ip" varchar(100) COLLATE "default" NOT NULL,
"create_date" timestamp(6) NOT NULL,
"re_user_id" varchar(64) COLLATE "default",
"re_date" timestamp(6),
"re_content" varchar(100) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."cms_guestbook" IS '留言板';

COMMENT ON COLUMN "public"."cms_guestbook"."id" IS '编号';

COMMENT ON COLUMN "public"."cms_guestbook"."type" IS '留言分类';

COMMENT ON COLUMN "public"."cms_guestbook"."content" IS '留言内容';

COMMENT ON COLUMN "public"."cms_guestbook"."name" IS '姓名';

COMMENT ON COLUMN "public"."cms_guestbook"."email" IS '邮箱';

COMMENT ON COLUMN "public"."cms_guestbook"."phone" IS '电话';

COMMENT ON COLUMN "public"."cms_guestbook"."workunit" IS '单位';

COMMENT ON COLUMN "public"."cms_guestbook"."ip" IS 'IP';

COMMENT ON COLUMN "public"."cms_guestbook"."create_date" IS '留言时间';

COMMENT ON COLUMN "public"."cms_guestbook"."re_user_id" IS '回复人';

COMMENT ON COLUMN "public"."cms_guestbook"."re_date" IS '回复时间';

COMMENT ON COLUMN "public"."cms_guestbook"."re_content" IS '回复内容';

COMMENT ON COLUMN "public"."cms_guestbook"."del_flag" IS '删除标记';;

-- ----------------------------
--  Table structure for "public"."cms_link"
-- ----------------------------
DROP TABLE "public"."cms_link";
CREATE TABLE "public"."cms_link" (
"id" varchar(64) COLLATE "default" NOT NULL,
"category_id" varchar(64) COLLATE "default" NOT NULL,
"title" varchar(255) COLLATE "default" NOT NULL,
"color" varchar(50) COLLATE "default",
"image" varchar(255) COLLATE "default",
"href" varchar(255) COLLATE "default",
"weight" int4,
"weight_date" timestamp(6),
"create_by" varchar(64) COLLATE "default",
"create_date" timestamp(6),
"update_by" varchar(64) COLLATE "default",
"update_date" timestamp(6),
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."cms_link" IS '友情链接';

COMMENT ON COLUMN "public"."cms_link"."id" IS '编号';

COMMENT ON COLUMN "public"."cms_link"."category_id" IS '栏目编号';

COMMENT ON COLUMN "public"."cms_link"."title" IS '链接名称';

COMMENT ON COLUMN "public"."cms_link"."color" IS '标题颜色';

COMMENT ON COLUMN "public"."cms_link"."image" IS '链接图片';

COMMENT ON COLUMN "public"."cms_link"."href" IS '链接地址';

COMMENT ON COLUMN "public"."cms_link"."weight" IS '权重，越大越靠前';

COMMENT ON COLUMN "public"."cms_link"."weight_date" IS '权重期限';

COMMENT ON COLUMN "public"."cms_link"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."cms_link"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."cms_link"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."cms_link"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."cms_link"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."cms_link"."del_flag" IS '删除标记';;

-- ----------------------------
--  Table structure for "public"."cms_site"
-- ----------------------------
DROP TABLE "public"."cms_site";
CREATE TABLE "public"."cms_site" (
"id" varchar(64) COLLATE "default" NOT NULL,
"name" varchar(100) COLLATE "default" NOT NULL,
"title" varchar(100) COLLATE "default" NOT NULL,
"logo" varchar(255) COLLATE "default",
"domain" varchar(255) COLLATE "default",
"description" varchar(255) COLLATE "default",
"keywords" varchar(255) COLLATE "default",
"theme" varchar(255) COLLATE "default",
"copyright" text COLLATE "default",
"custom_index_view" varchar(255) COLLATE "default",
"create_by" varchar(64) COLLATE "default",
"create_date" timestamp(6),
"update_by" varchar(64) COLLATE "default",
"update_date" timestamp(6),
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."cms_site" IS '站点表';

COMMENT ON COLUMN "public"."cms_site"."id" IS '编号';

COMMENT ON COLUMN "public"."cms_site"."name" IS '站点名称';

COMMENT ON COLUMN "public"."cms_site"."title" IS '站点标题';

COMMENT ON COLUMN "public"."cms_site"."logo" IS '站点Logo';

COMMENT ON COLUMN "public"."cms_site"."domain" IS '站点域名';

COMMENT ON COLUMN "public"."cms_site"."description" IS '描述';

COMMENT ON COLUMN "public"."cms_site"."keywords" IS '关键字';

COMMENT ON COLUMN "public"."cms_site"."theme" IS '主题';

COMMENT ON COLUMN "public"."cms_site"."copyright" IS '版权信息';

COMMENT ON COLUMN "public"."cms_site"."custom_index_view" IS '自定义站点首页视图';

COMMENT ON COLUMN "public"."cms_site"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."cms_site"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."cms_site"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."cms_site"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."cms_site"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."cms_site"."del_flag" IS '删除标记';;

-- ----------------------------
--  Table structure for "public"."gen_scheme"
-- ----------------------------
DROP TABLE "public"."gen_scheme";
CREATE TABLE "public"."gen_scheme" (
"id" varchar(64) COLLATE "default" NOT NULL,
"name" varchar(200) COLLATE "default",
"category" varchar(2000) COLLATE "default",
"package_name" varchar(500) COLLATE "default",
"module_name" varchar(30) COLLATE "default",
"sub_module_name" varchar(30) COLLATE "default",
"function_name" varchar(500) COLLATE "default",
"function_name_simple" varchar(100) COLLATE "default",
"function_author" varchar(100) COLLATE "default",
"gen_table_id" varchar(200) COLLATE "default",
"create_by" varchar(64) COLLATE "default",
"create_date" timestamp(6),
"update_by" varchar(64) COLLATE "default",
"update_date" timestamp(6),
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."gen_scheme" IS '生成方案';

COMMENT ON COLUMN "public"."gen_scheme"."id" IS '编号';

COMMENT ON COLUMN "public"."gen_scheme"."name" IS '名称';

COMMENT ON COLUMN "public"."gen_scheme"."category" IS '分类';

COMMENT ON COLUMN "public"."gen_scheme"."package_name" IS '生成包路径';

COMMENT ON COLUMN "public"."gen_scheme"."module_name" IS '生成模块名';

COMMENT ON COLUMN "public"."gen_scheme"."sub_module_name" IS '生成子模块名';

COMMENT ON COLUMN "public"."gen_scheme"."function_name" IS '生成功能名';

COMMENT ON COLUMN "public"."gen_scheme"."function_name_simple" IS '生成功能名（简写）';

COMMENT ON COLUMN "public"."gen_scheme"."function_author" IS '生成功能作者';

COMMENT ON COLUMN "public"."gen_scheme"."gen_table_id" IS '生成表编号';

COMMENT ON COLUMN "public"."gen_scheme"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."gen_scheme"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."gen_scheme"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."gen_scheme"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."gen_scheme"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."gen_scheme"."del_flag" IS '删除标记（0：正常；1：删除）';;

-- ----------------------------
--  Table structure for "public"."gen_table"
-- ----------------------------
DROP TABLE "public"."gen_table";
CREATE TABLE "public"."gen_table" (
"id" varchar(64) COLLATE "default" NOT NULL,
"name" varchar(200) COLLATE "default",
"comments" varchar(500) COLLATE "default",
"class_name" varchar(100) COLLATE "default",
"parent_table" varchar(200) COLLATE "default",
"parent_table_fk" varchar(100) COLLATE "default",
"create_by" varchar(64) COLLATE "default",
"create_date" timestamp(6),
"update_by" varchar(64) COLLATE "default",
"update_date" timestamp(6),
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."gen_table" IS '业务表';

COMMENT ON COLUMN "public"."gen_table"."id" IS '编号';

COMMENT ON COLUMN "public"."gen_table"."name" IS '名称';

COMMENT ON COLUMN "public"."gen_table"."comments" IS '描述';

COMMENT ON COLUMN "public"."gen_table"."class_name" IS '实体类名称';

COMMENT ON COLUMN "public"."gen_table"."parent_table" IS '关联父表';

COMMENT ON COLUMN "public"."gen_table"."parent_table_fk" IS '关联父表外键';

COMMENT ON COLUMN "public"."gen_table"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."gen_table"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."gen_table"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."gen_table"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."gen_table"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."gen_table"."del_flag" IS '删除标记（0：正常；1：删除）';;

-- ----------------------------
--  Table structure for "public"."gen_table_column"
-- ----------------------------
DROP TABLE "public"."gen_table_column";
CREATE TABLE "public"."gen_table_column" (
"id" varchar(64) COLLATE "default" NOT NULL,
"gen_table_id" varchar(64) COLLATE "default",
"name" varchar(200) COLLATE "default",
"comments" varchar(500) COLLATE "default",
"jdbc_type" varchar(100) COLLATE "default",
"java_type" varchar(500) COLLATE "default",
"java_field" varchar(200) COLLATE "default",
"is_pk" char(1) COLLATE "default",
"is_null" char(1) COLLATE "default",
"is_insert" char(1) COLLATE "default",
"is_edit" char(1) COLLATE "default",
"is_list" char(1) COLLATE "default",
"is_query" char(1) COLLATE "default",
"query_type" varchar(200) COLLATE "default",
"show_type" varchar(200) COLLATE "default",
"dict_type" varchar(200) COLLATE "default",
"settings" varchar(2000) COLLATE "default",
"sort" numeric(10),
"create_by" varchar(64) COLLATE "default",
"create_date" timestamp(6),
"update_by" varchar(64) COLLATE "default",
"update_date" timestamp(6),
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."gen_table_column" IS '业务表字段';

COMMENT ON COLUMN "public"."gen_table_column"."id" IS '编号';

COMMENT ON COLUMN "public"."gen_table_column"."gen_table_id" IS '归属表编号';

COMMENT ON COLUMN "public"."gen_table_column"."name" IS '名称';

COMMENT ON COLUMN "public"."gen_table_column"."comments" IS '描述';

COMMENT ON COLUMN "public"."gen_table_column"."jdbc_type" IS '列的数据类型的字节长度';

COMMENT ON COLUMN "public"."gen_table_column"."java_type" IS 'JAVA类型';

COMMENT ON COLUMN "public"."gen_table_column"."java_field" IS 'JAVA字段名';

COMMENT ON COLUMN "public"."gen_table_column"."is_pk" IS '是否主键';

COMMENT ON COLUMN "public"."gen_table_column"."is_null" IS '是否可为空';

COMMENT ON COLUMN "public"."gen_table_column"."is_insert" IS '是否为插入字段';

COMMENT ON COLUMN "public"."gen_table_column"."is_edit" IS '是否编辑字段';

COMMENT ON COLUMN "public"."gen_table_column"."is_list" IS '是否列表字段';

COMMENT ON COLUMN "public"."gen_table_column"."is_query" IS '是否查询字段';

COMMENT ON COLUMN "public"."gen_table_column"."query_type" IS '查询方式（等于、不等于、大于、小于、范围、左LIKE、右LIKE、左右LIKE）';

COMMENT ON COLUMN "public"."gen_table_column"."show_type" IS '字段生成方案（文本框、文本域、下拉框、复选框、单选框、字典选择、人员选择、部门选择、区域选择）';

COMMENT ON COLUMN "public"."gen_table_column"."dict_type" IS '字典类型';

COMMENT ON COLUMN "public"."gen_table_column"."settings" IS '其它设置（扩展字段JSON）';

COMMENT ON COLUMN "public"."gen_table_column"."sort" IS '排序（升序）';

COMMENT ON COLUMN "public"."gen_table_column"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."gen_table_column"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."gen_table_column"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."gen_table_column"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."gen_table_column"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."gen_table_column"."del_flag" IS '删除标记（0：正常；1：删除）';;

-- ----------------------------
--  Table structure for "public"."gen_template"
-- ----------------------------
DROP TABLE "public"."gen_template";
CREATE TABLE "public"."gen_template" (
"id" varchar(64) COLLATE "default" NOT NULL,
"name" varchar(200) COLLATE "default",
"category" varchar(2000) COLLATE "default",
"file_path" varchar(500) COLLATE "default",
"file_name" varchar(200) COLLATE "default",
"content" text COLLATE "default",
"create_by" varchar(64) COLLATE "default",
"create_date" timestamp(6),
"update_by" varchar(64) COLLATE "default",
"update_date" timestamp(6),
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."gen_template" IS '代码模板表';

COMMENT ON COLUMN "public"."gen_template"."id" IS '编号';

COMMENT ON COLUMN "public"."gen_template"."name" IS '名称';

COMMENT ON COLUMN "public"."gen_template"."category" IS '分类';

COMMENT ON COLUMN "public"."gen_template"."file_path" IS '生成文件路径';

COMMENT ON COLUMN "public"."gen_template"."file_name" IS '生成文件名';

COMMENT ON COLUMN "public"."gen_template"."content" IS '内容';

COMMENT ON COLUMN "public"."gen_template"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."gen_template"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."gen_template"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."gen_template"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."gen_template"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."gen_template"."del_flag" IS '删除标记（0：正常；1：删除）';;

-- ----------------------------
--  Table structure for "public"."oa_leave"
-- ----------------------------
DROP TABLE "public"."oa_leave";
CREATE TABLE "public"."oa_leave" (
"id" varchar(64) COLLATE "default" NOT NULL,
"process_instance_id" varchar(64) COLLATE "default",
"start_time" timestamp(6),
"end_time" timestamp(6),
"leave_type" varchar(20) COLLATE "default",
"reason" varchar(255) COLLATE "default",
"apply_time" timestamp(6),
"reality_start_time" timestamp(6),
"reality_end_time" timestamp(6),
"create_by" varchar(64) COLLATE "default" NOT NULL,
"create_date" timestamp(6) NOT NULL,
"update_by" varchar(64) COLLATE "default" NOT NULL,
"update_date" timestamp(6) NOT NULL,
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."oa_leave" IS '请假流程表';

COMMENT ON COLUMN "public"."oa_leave"."id" IS '编号';

COMMENT ON COLUMN "public"."oa_leave"."process_instance_id" IS '流程实例编号';

COMMENT ON COLUMN "public"."oa_leave"."start_time" IS '开始时间';

COMMENT ON COLUMN "public"."oa_leave"."end_time" IS '结束时间';

COMMENT ON COLUMN "public"."oa_leave"."leave_type" IS '请假类型';

COMMENT ON COLUMN "public"."oa_leave"."reason" IS '请假理由';

COMMENT ON COLUMN "public"."oa_leave"."apply_time" IS '申请时间';

COMMENT ON COLUMN "public"."oa_leave"."reality_start_time" IS '实际开始时间';

COMMENT ON COLUMN "public"."oa_leave"."reality_end_time" IS '实际结束时间';

COMMENT ON COLUMN "public"."oa_leave"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."oa_leave"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."oa_leave"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."oa_leave"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."oa_leave"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."oa_leave"."del_flag" IS '删除标记';;

-- ----------------------------
--  Table structure for "public"."oa_notify"
-- ----------------------------
DROP TABLE "public"."oa_notify";
CREATE TABLE "public"."oa_notify" (
"id" varchar(64) COLLATE "default" NOT NULL,
"type" char(1) COLLATE "default",
"title" varchar(200) COLLATE "default",
"content" varchar(2000) COLLATE "default",
"files" varchar(2000) COLLATE "default",
"status" char(1) COLLATE "default",
"create_by" varchar(64) COLLATE "default" NOT NULL,
"create_date" timestamp(6) NOT NULL,
"update_by" varchar(64) COLLATE "default" NOT NULL,
"update_date" timestamp(6) NOT NULL,
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."oa_notify" IS '通知通告';

COMMENT ON COLUMN "public"."oa_notify"."id" IS '编号';

COMMENT ON COLUMN "public"."oa_notify"."type" IS '类型';

COMMENT ON COLUMN "public"."oa_notify"."title" IS '标题';

COMMENT ON COLUMN "public"."oa_notify"."content" IS '内容';

COMMENT ON COLUMN "public"."oa_notify"."files" IS '附件';

COMMENT ON COLUMN "public"."oa_notify"."status" IS '状态';

COMMENT ON COLUMN "public"."oa_notify"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."oa_notify"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."oa_notify"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."oa_notify"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."oa_notify"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."oa_notify"."del_flag" IS '删除标记';;

-- ----------------------------
--  Table structure for "public"."oa_notify_record"
-- ----------------------------
DROP TABLE "public"."oa_notify_record";
CREATE TABLE "public"."oa_notify_record" (
"id" varchar(64) COLLATE "default" NOT NULL,
"oa_notify_id" varchar(64) COLLATE "default",
"user_id" varchar(64) COLLATE "default",
"read_flag" char(1) COLLATE "default",
"read_date" date
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."oa_notify_record" IS '通知通告发送记录';

COMMENT ON COLUMN "public"."oa_notify_record"."id" IS '编号';

COMMENT ON COLUMN "public"."oa_notify_record"."oa_notify_id" IS '通知通告ID';

COMMENT ON COLUMN "public"."oa_notify_record"."user_id" IS '接受人';

COMMENT ON COLUMN "public"."oa_notify_record"."read_flag" IS '阅读标记';

COMMENT ON COLUMN "public"."oa_notify_record"."read_date" IS '阅读时间';;

-- ----------------------------
--  Table structure for "public"."oa_test_audit"
-- ----------------------------
DROP TABLE "public"."oa_test_audit";
CREATE TABLE "public"."oa_test_audit" (
"id" varchar(64) COLLATE "default" NOT NULL,
"PROC_INS_ID" varchar(64) COLLATE "default",
"USER_ID" varchar(64) COLLATE "default",
"OFFICE_ID" varchar(64) COLLATE "default",
"POST" varchar(255) COLLATE "default",
"AGE" char(1) COLLATE "default",
"EDU" varchar(255) COLLATE "default",
"CONTENT" varchar(255) COLLATE "default",
"OLDA" varchar(255) COLLATE "default",
"OLDB" varchar(255) COLLATE "default",
"OLDC" varchar(255) COLLATE "default",
"NEWA" varchar(255) COLLATE "default",
"NEWB" varchar(255) COLLATE "default",
"NEWC" varchar(255) COLLATE "default",
"ADD_NUM" varchar(255) COLLATE "default",
"EXE_DATE" varchar(255) COLLATE "default",
"HR_TEXT" varchar(255) COLLATE "default",
"LEAD_TEXT" varchar(255) COLLATE "default",
"MAIN_LEAD_TEXT" varchar(255) COLLATE "default",
"create_by" varchar(64) COLLATE "default" NOT NULL,
"create_date" timestamp(6) NOT NULL,
"update_by" varchar(64) COLLATE "default" NOT NULL,
"update_date" timestamp(6) NOT NULL,
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."oa_test_audit" IS '审批流程测试表';

COMMENT ON COLUMN "public"."oa_test_audit"."id" IS '编号';

COMMENT ON COLUMN "public"."oa_test_audit"."PROC_INS_ID" IS '流程实例ID';

COMMENT ON COLUMN "public"."oa_test_audit"."USER_ID" IS '变动用户';

COMMENT ON COLUMN "public"."oa_test_audit"."OFFICE_ID" IS '归属部门';

COMMENT ON COLUMN "public"."oa_test_audit"."POST" IS '岗位';

COMMENT ON COLUMN "public"."oa_test_audit"."AGE" IS '性别';

COMMENT ON COLUMN "public"."oa_test_audit"."EDU" IS '学历';

COMMENT ON COLUMN "public"."oa_test_audit"."CONTENT" IS '调整原因';

COMMENT ON COLUMN "public"."oa_test_audit"."OLDA" IS '现行标准 薪酬档级';

COMMENT ON COLUMN "public"."oa_test_audit"."OLDB" IS '现行标准 月工资额';

COMMENT ON COLUMN "public"."oa_test_audit"."OLDC" IS '现行标准 年薪总额';

COMMENT ON COLUMN "public"."oa_test_audit"."NEWA" IS '调整后标准 薪酬档级';

COMMENT ON COLUMN "public"."oa_test_audit"."NEWB" IS '调整后标准 月工资额';

COMMENT ON COLUMN "public"."oa_test_audit"."NEWC" IS '调整后标准 年薪总额';

COMMENT ON COLUMN "public"."oa_test_audit"."ADD_NUM" IS '月增资';

COMMENT ON COLUMN "public"."oa_test_audit"."EXE_DATE" IS '执行时间';

COMMENT ON COLUMN "public"."oa_test_audit"."HR_TEXT" IS '人力资源部门意见';

COMMENT ON COLUMN "public"."oa_test_audit"."LEAD_TEXT" IS '分管领导意见';

COMMENT ON COLUMN "public"."oa_test_audit"."MAIN_LEAD_TEXT" IS '集团主要领导意见';

COMMENT ON COLUMN "public"."oa_test_audit"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."oa_test_audit"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."oa_test_audit"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."oa_test_audit"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."oa_test_audit"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."oa_test_audit"."del_flag" IS '删除标记';;

-- ----------------------------
--  Table structure for "public"."sys_area"
-- ----------------------------
DROP TABLE "public"."sys_area";
CREATE TABLE "public"."sys_area" (
"id" varchar(64) COLLATE "default" NOT NULL,
"parent_id" varchar(64) COLLATE "default" NOT NULL,
"parent_ids" varchar(2000) COLLATE "default" NOT NULL,
"name" varchar(100) COLLATE "default" NOT NULL,
"sort" numeric(10) NOT NULL,
"code" varchar(100) COLLATE "default",
"type" char(1) COLLATE "default",
"create_by" varchar(64) COLLATE "default" NOT NULL,
"create_date" timestamp(6) NOT NULL,
"update_by" varchar(64) COLLATE "default" NOT NULL,
"update_date" timestamp(6) NOT NULL,
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."sys_area" IS '区域表';

COMMENT ON COLUMN "public"."sys_area"."id" IS '编号';

COMMENT ON COLUMN "public"."sys_area"."parent_id" IS '父级编号';

COMMENT ON COLUMN "public"."sys_area"."parent_ids" IS '所有父级编号';

COMMENT ON COLUMN "public"."sys_area"."name" IS '名称';

COMMENT ON COLUMN "public"."sys_area"."sort" IS '排序';

COMMENT ON COLUMN "public"."sys_area"."code" IS '区域编码';

COMMENT ON COLUMN "public"."sys_area"."type" IS '区域类型';

COMMENT ON COLUMN "public"."sys_area"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."sys_area"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."sys_area"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."sys_area"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."sys_area"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."sys_area"."del_flag" IS '删除标记';;

-- ----------------------------
--  Table structure for "public"."sys_dict"
-- ----------------------------
DROP TABLE "public"."sys_dict";
CREATE TABLE "public"."sys_dict" (
"id" varchar(64) COLLATE "default" NOT NULL,
"value" varchar(100) COLLATE "default" NOT NULL,
"label" varchar(100) COLLATE "default" NOT NULL,
"type" varchar(100) COLLATE "default" NOT NULL,
"description" varchar(100) COLLATE "default" NOT NULL,
"sort" numeric(10) NOT NULL,
"parent_id" varchar(64) COLLATE "default",
"create_by" varchar(64) COLLATE "default" NOT NULL,
"create_date" timestamp(6) NOT NULL,
"update_by" varchar(64) COLLATE "default" NOT NULL,
"update_date" timestamp(6) NOT NULL,
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."sys_dict" IS '字典表';

COMMENT ON COLUMN "public"."sys_dict"."id" IS '编号';

COMMENT ON COLUMN "public"."sys_dict"."value" IS '数据值';

COMMENT ON COLUMN "public"."sys_dict"."label" IS '标签名';

COMMENT ON COLUMN "public"."sys_dict"."type" IS '类型';

COMMENT ON COLUMN "public"."sys_dict"."description" IS '描述';

COMMENT ON COLUMN "public"."sys_dict"."sort" IS '排序（升序）';

COMMENT ON COLUMN "public"."sys_dict"."parent_id" IS '父级编号';

COMMENT ON COLUMN "public"."sys_dict"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."sys_dict"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."sys_dict"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."sys_dict"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."sys_dict"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."sys_dict"."del_flag" IS '删除标记';;

-- ----------------------------
--  Table structure for "public"."sys_log"
-- ----------------------------
DROP TABLE "public"."sys_log";
CREATE TABLE "public"."sys_log" (
"id" varchar(64) COLLATE "default" NOT NULL,
"type" char(1) COLLATE "default",
"title" varchar(255) COLLATE "default",
"create_by" varchar(64) COLLATE "default",
"create_date" timestamp(6),
"remote_addr" varchar(255) COLLATE "default",
"user_agent" varchar(255) COLLATE "default",
"request_uri" varchar(255) COLLATE "default",
"method" varchar(5) COLLATE "default",
"params" text COLLATE "default",
"exception" text COLLATE "default"
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."sys_log" IS '日志表';

COMMENT ON COLUMN "public"."sys_log"."id" IS '编号';

COMMENT ON COLUMN "public"."sys_log"."type" IS '日志类型';

COMMENT ON COLUMN "public"."sys_log"."title" IS '日志标题';

COMMENT ON COLUMN "public"."sys_log"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."sys_log"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."sys_log"."remote_addr" IS '操作IP地址';

COMMENT ON COLUMN "public"."sys_log"."user_agent" IS '用户代理';

COMMENT ON COLUMN "public"."sys_log"."request_uri" IS '请求URI';

COMMENT ON COLUMN "public"."sys_log"."method" IS '操作方式';

COMMENT ON COLUMN "public"."sys_log"."params" IS '操作提交的数据';

COMMENT ON COLUMN "public"."sys_log"."exception" IS '异常信息';;

-- ----------------------------
--  Table structure for "public"."sys_mdict"
-- ----------------------------
DROP TABLE "public"."sys_mdict";
CREATE TABLE "public"."sys_mdict" (
"id" varchar(64) COLLATE "default" NOT NULL,
"parent_id" varchar(64) COLLATE "default" NOT NULL,
"parent_ids" varchar(2000) COLLATE "default" NOT NULL,
"name" varchar(100) COLLATE "default" NOT NULL,
"sort" numeric(10) NOT NULL,
"description" varchar(100) COLLATE "default",
"create_by" varchar(64) COLLATE "default" NOT NULL,
"create_date" timestamp(6) NOT NULL,
"update_by" varchar(64) COLLATE "default" NOT NULL,
"update_date" timestamp(6) NOT NULL,
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."sys_mdict" IS '多级字典表';

COMMENT ON COLUMN "public"."sys_mdict"."id" IS '编号';

COMMENT ON COLUMN "public"."sys_mdict"."parent_id" IS '父级编号';

COMMENT ON COLUMN "public"."sys_mdict"."parent_ids" IS '所有父级编号';

COMMENT ON COLUMN "public"."sys_mdict"."name" IS '名称';

COMMENT ON COLUMN "public"."sys_mdict"."sort" IS '排序';

COMMENT ON COLUMN "public"."sys_mdict"."description" IS '描述';

COMMENT ON COLUMN "public"."sys_mdict"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."sys_mdict"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."sys_mdict"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."sys_mdict"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."sys_mdict"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."sys_mdict"."del_flag" IS '删除标记';;

-- ----------------------------
--  Table structure for "public"."sys_menu"
-- ----------------------------
DROP TABLE "public"."sys_menu";
CREATE TABLE "public"."sys_menu" (
"id" varchar(64) COLLATE "default" NOT NULL,
"parent_id" varchar(64) COLLATE "default" NOT NULL,
"parent_ids" varchar(2000) COLLATE "default" NOT NULL,
"name" varchar(100) COLLATE "default" NOT NULL,
"sort" numeric(10) NOT NULL,
"href" varchar(2000) COLLATE "default",
"target" varchar(20) COLLATE "default",
"icon" varchar(100) COLLATE "default",
"is_show" char(1) COLLATE "default" NOT NULL,
"permission" varchar(200) COLLATE "default",
"create_by" varchar(64) COLLATE "default" NOT NULL,
"create_date" timestamp(6) NOT NULL,
"update_by" varchar(64) COLLATE "default" NOT NULL,
"update_date" timestamp(6) NOT NULL,
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."sys_menu" IS '菜单表';

COMMENT ON COLUMN "public"."sys_menu"."id" IS '编号';

COMMENT ON COLUMN "public"."sys_menu"."parent_id" IS '父级编号';

COMMENT ON COLUMN "public"."sys_menu"."parent_ids" IS '所有父级编号';

COMMENT ON COLUMN "public"."sys_menu"."name" IS '名称';

COMMENT ON COLUMN "public"."sys_menu"."sort" IS '排序';

COMMENT ON COLUMN "public"."sys_menu"."href" IS '链接';

COMMENT ON COLUMN "public"."sys_menu"."target" IS '目标';

COMMENT ON COLUMN "public"."sys_menu"."icon" IS '图标';

COMMENT ON COLUMN "public"."sys_menu"."is_show" IS '是否在菜单中显示';

COMMENT ON COLUMN "public"."sys_menu"."permission" IS '权限标识';

COMMENT ON COLUMN "public"."sys_menu"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."sys_menu"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."sys_menu"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."sys_menu"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."sys_menu"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."sys_menu"."del_flag" IS '删除标记';;

-- ----------------------------
--  Table structure for "public"."sys_office"
-- ----------------------------
DROP TABLE "public"."sys_office";
CREATE TABLE "public"."sys_office" (
"id" varchar(64) COLLATE "default" NOT NULL,
"parent_id" varchar(64) COLLATE "default" NOT NULL,
"parent_ids" varchar(2000) COLLATE "default" NOT NULL,
"name" varchar(100) COLLATE "default" NOT NULL,
"sort" numeric(10) NOT NULL,
"area_id" varchar(64) COLLATE "default" NOT NULL,
"code" varchar(100) COLLATE "default",
"type" char(1) COLLATE "default" NOT NULL,
"grade" char(1) COLLATE "default" NOT NULL,
"address" varchar(255) COLLATE "default",
"zip_code" varchar(100) COLLATE "default",
"master" varchar(100) COLLATE "default",
"phone" varchar(200) COLLATE "default",
"fax" varchar(200) COLLATE "default",
"email" varchar(200) COLLATE "default",
"useable" varchar(64) COLLATE "default",
"primary_person" varchar(64) COLLATE "default",
"deputy_person" varchar(64) COLLATE "default",
"create_by" varchar(64) COLLATE "default" NOT NULL,
"create_date" timestamp(6) NOT NULL,
"update_by" varchar(64) COLLATE "default" NOT NULL,
"update_date" timestamp(6) NOT NULL,
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."sys_office" IS '机构表';

COMMENT ON COLUMN "public"."sys_office"."id" IS '编号';

COMMENT ON COLUMN "public"."sys_office"."parent_id" IS '父级编号';

COMMENT ON COLUMN "public"."sys_office"."parent_ids" IS '所有父级编号';

COMMENT ON COLUMN "public"."sys_office"."name" IS '名称';

COMMENT ON COLUMN "public"."sys_office"."sort" IS '排序';

COMMENT ON COLUMN "public"."sys_office"."area_id" IS '归属区域';

COMMENT ON COLUMN "public"."sys_office"."code" IS '区域编码';

COMMENT ON COLUMN "public"."sys_office"."type" IS '机构类型';

COMMENT ON COLUMN "public"."sys_office"."grade" IS '机构等级';

COMMENT ON COLUMN "public"."sys_office"."address" IS '联系地址';

COMMENT ON COLUMN "public"."sys_office"."zip_code" IS '邮政编码';

COMMENT ON COLUMN "public"."sys_office"."master" IS '负责人';

COMMENT ON COLUMN "public"."sys_office"."phone" IS '电话';

COMMENT ON COLUMN "public"."sys_office"."fax" IS '传真';

COMMENT ON COLUMN "public"."sys_office"."email" IS '邮箱';

COMMENT ON COLUMN "public"."sys_office"."useable" IS '是否启用';

COMMENT ON COLUMN "public"."sys_office"."primary_person" IS '主负责人';

COMMENT ON COLUMN "public"."sys_office"."deputy_person" IS '副负责人';

COMMENT ON COLUMN "public"."sys_office"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."sys_office"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."sys_office"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."sys_office"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."sys_office"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."sys_office"."del_flag" IS '删除标记';;

-- ----------------------------
--  Table structure for "public"."sys_role"
-- ----------------------------
DROP TABLE "public"."sys_role";
CREATE TABLE "public"."sys_role" (
"id" varchar(64) COLLATE "default" NOT NULL,
"office_id" varchar(64) COLLATE "default",
"name" varchar(100) COLLATE "default" NOT NULL,
"enname" varchar(255) COLLATE "default",
"role_type" varchar(255) COLLATE "default",
"data_scope" char(1) COLLATE "default",
"is_sys" varchar(64) COLLATE "default",
"useable" varchar(64) COLLATE "default",
"create_by" varchar(64) COLLATE "default" NOT NULL,
"create_date" timestamp(6) NOT NULL,
"update_by" varchar(64) COLLATE "default" NOT NULL,
"update_date" timestamp(6) NOT NULL,
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."sys_role" IS '角色表';

COMMENT ON COLUMN "public"."sys_role"."id" IS '编号';

COMMENT ON COLUMN "public"."sys_role"."office_id" IS '归属机构';

COMMENT ON COLUMN "public"."sys_role"."name" IS '角色名称';

COMMENT ON COLUMN "public"."sys_role"."enname" IS '英文名称';

COMMENT ON COLUMN "public"."sys_role"."role_type" IS '角色类型';

COMMENT ON COLUMN "public"."sys_role"."data_scope" IS '数据范围';

COMMENT ON COLUMN "public"."sys_role"."is_sys" IS '是否系统数据';

COMMENT ON COLUMN "public"."sys_role"."useable" IS '是否可用';

COMMENT ON COLUMN "public"."sys_role"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."sys_role"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."sys_role"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."sys_role"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."sys_role"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."sys_role"."del_flag" IS '删除标记';;

-- ----------------------------
--  Table structure for "public"."sys_role_menu"
-- ----------------------------
DROP TABLE "public"."sys_role_menu";
CREATE TABLE "public"."sys_role_menu" (
"role_id" varchar(64) COLLATE "default" NOT NULL,
"menu_id" varchar(64) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."sys_role_menu" IS '角色-菜单';

COMMENT ON COLUMN "public"."sys_role_menu"."role_id" IS '角色编号';

COMMENT ON COLUMN "public"."sys_role_menu"."menu_id" IS '菜单编号';;

-- ----------------------------
--  Table structure for "public"."sys_role_office"
-- ----------------------------
DROP TABLE "public"."sys_role_office";
CREATE TABLE "public"."sys_role_office" (
"role_id" varchar(64) COLLATE "default" NOT NULL,
"office_id" varchar(64) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."sys_role_office" IS '角色-机构';

COMMENT ON COLUMN "public"."sys_role_office"."role_id" IS '角色编号';

COMMENT ON COLUMN "public"."sys_role_office"."office_id" IS '机构编号';;

-- ----------------------------
--  Table structure for "public"."sys_user"
-- ----------------------------
DROP TABLE "public"."sys_user";
CREATE TABLE "public"."sys_user" (
"id" varchar(64) COLLATE "default" NOT NULL,
"company_id" varchar(64) COLLATE "default" NOT NULL,
"office_id" varchar(64) COLLATE "default" NOT NULL,
"login_name" varchar(100) COLLATE "default" NOT NULL,
"password" varchar(100) COLLATE "default" NOT NULL,
"no" varchar(100) COLLATE "default",
"name" varchar(100) COLLATE "default" NOT NULL,
"email" varchar(200) COLLATE "default",
"phone" varchar(200) COLLATE "default",
"mobile" varchar(200) COLLATE "default",
"user_type" char(1) COLLATE "default",
"photo" varchar(1000) COLLATE "default",
"login_ip" varchar(100) COLLATE "default",
"login_date" timestamp(6),
"login_flag" varchar(64) COLLATE "default",
"create_by" varchar(64) COLLATE "default" NOT NULL,
"create_date" timestamp(6) NOT NULL,
"update_by" varchar(64) COLLATE "default" NOT NULL,
"update_date" timestamp(6) NOT NULL,
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."sys_user" IS '用户表';

COMMENT ON COLUMN "public"."sys_user"."id" IS '编号';

COMMENT ON COLUMN "public"."sys_user"."company_id" IS '归属公司';

COMMENT ON COLUMN "public"."sys_user"."office_id" IS '归属部门';

COMMENT ON COLUMN "public"."sys_user"."login_name" IS '登录名';

COMMENT ON COLUMN "public"."sys_user"."password" IS '密码';

COMMENT ON COLUMN "public"."sys_user"."no" IS '工号';

COMMENT ON COLUMN "public"."sys_user"."name" IS '姓名';

COMMENT ON COLUMN "public"."sys_user"."email" IS '邮箱';

COMMENT ON COLUMN "public"."sys_user"."phone" IS '电话';

COMMENT ON COLUMN "public"."sys_user"."mobile" IS '手机';

COMMENT ON COLUMN "public"."sys_user"."user_type" IS '用户类型';

COMMENT ON COLUMN "public"."sys_user"."photo" IS '用户头像';

COMMENT ON COLUMN "public"."sys_user"."login_ip" IS '最后登陆IP';

COMMENT ON COLUMN "public"."sys_user"."login_date" IS '最后登陆时间';

COMMENT ON COLUMN "public"."sys_user"."login_flag" IS '是否可登录';

COMMENT ON COLUMN "public"."sys_user"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."sys_user"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."sys_user"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."sys_user"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."sys_user"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."sys_user"."del_flag" IS '删除标记';;

-- ----------------------------
--  Table structure for "public"."sys_user_role"
-- ----------------------------
DROP TABLE "public"."sys_user_role";
CREATE TABLE "public"."sys_user_role" (
"user_id" varchar(64) COLLATE "default" NOT NULL,
"role_id" varchar(64) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."sys_user_role" IS '用户-角色';

COMMENT ON COLUMN "public"."sys_user_role"."user_id" IS '用户编号';

COMMENT ON COLUMN "public"."sys_user_role"."role_id" IS '角色编号';;

-- ----------------------------
--  Table structure for "public"."test_data"
-- ----------------------------
DROP TABLE "public"."test_data";
CREATE TABLE "public"."test_data" (
"id" varchar(64) COLLATE "default" NOT NULL,
"user_id" varchar(64) COLLATE "default",
"office_id" varchar(64) COLLATE "default",
"area_id" varchar(64) COLLATE "default",
"name" varchar(100) COLLATE "default",
"sex" char(1) COLLATE "default",
"in_date" date,
"create_by" varchar(64) COLLATE "default" NOT NULL,
"create_date" timestamp(6) NOT NULL,
"update_by" varchar(64) COLLATE "default" NOT NULL,
"update_date" timestamp(6) NOT NULL,
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."test_data" IS '业务数据表';

COMMENT ON COLUMN "public"."test_data"."id" IS '编号';

COMMENT ON COLUMN "public"."test_data"."user_id" IS '归属用户';

COMMENT ON COLUMN "public"."test_data"."office_id" IS '归属部门';

COMMENT ON COLUMN "public"."test_data"."area_id" IS '归属区域';

COMMENT ON COLUMN "public"."test_data"."name" IS '名称';

COMMENT ON COLUMN "public"."test_data"."sex" IS '性别';

COMMENT ON COLUMN "public"."test_data"."in_date" IS '加入日期';

COMMENT ON COLUMN "public"."test_data"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."test_data"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."test_data"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."test_data"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."test_data"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."test_data"."del_flag" IS '删除标记';;

-- ----------------------------
--  Table structure for "public"."test_data_child"
-- ----------------------------
DROP TABLE "public"."test_data_child";
CREATE TABLE "public"."test_data_child" (
"id" varchar(64) COLLATE "default" NOT NULL,
"test_data_main_id" varchar(64) COLLATE "default",
"name" varchar(100) COLLATE "default",
"create_by" varchar(64) COLLATE "default" NOT NULL,
"create_date" timestamp(6) NOT NULL,
"update_by" varchar(64) COLLATE "default" NOT NULL,
"update_date" timestamp(6) NOT NULL,
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."test_data_child" IS '业务数据子表';

COMMENT ON COLUMN "public"."test_data_child"."id" IS '编号';

COMMENT ON COLUMN "public"."test_data_child"."test_data_main_id" IS '业务主表ID';

COMMENT ON COLUMN "public"."test_data_child"."name" IS '名称';

COMMENT ON COLUMN "public"."test_data_child"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."test_data_child"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."test_data_child"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."test_data_child"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."test_data_child"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."test_data_child"."del_flag" IS '删除标记';;

-- ----------------------------
--  Table structure for "public"."test_data_main"
-- ----------------------------
DROP TABLE "public"."test_data_main";
CREATE TABLE "public"."test_data_main" (
"id" varchar(64) COLLATE "default" NOT NULL,
"user_id" varchar(64) COLLATE "default",
"office_id" varchar(64) COLLATE "default",
"area_id" varchar(64) COLLATE "default",
"name" varchar(100) COLLATE "default",
"sex" char(1) COLLATE "default",
"in_date" date,
"create_by" varchar(64) COLLATE "default" NOT NULL,
"create_date" timestamp(6) NOT NULL,
"update_by" varchar(64) COLLATE "default" NOT NULL,
"update_date" timestamp(6) NOT NULL,
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."test_data_main" IS '业务数据表';

COMMENT ON COLUMN "public"."test_data_main"."id" IS '编号';

COMMENT ON COLUMN "public"."test_data_main"."user_id" IS '归属用户';

COMMENT ON COLUMN "public"."test_data_main"."office_id" IS '归属部门';

COMMENT ON COLUMN "public"."test_data_main"."area_id" IS '归属区域';

COMMENT ON COLUMN "public"."test_data_main"."name" IS '名称';

COMMENT ON COLUMN "public"."test_data_main"."sex" IS '性别';

COMMENT ON COLUMN "public"."test_data_main"."in_date" IS '加入日期';

COMMENT ON COLUMN "public"."test_data_main"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."test_data_main"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."test_data_main"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."test_data_main"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."test_data_main"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."test_data_main"."del_flag" IS '删除标记';;

-- ----------------------------
--  Table structure for "public"."test_tree"
-- ----------------------------
DROP TABLE "public"."test_tree";
CREATE TABLE "public"."test_tree" (
"id" varchar(64) COLLATE "default" NOT NULL,
"parent_id" varchar(64) COLLATE "default" NOT NULL,
"parent_ids" varchar(2000) COLLATE "default" NOT NULL,
"name" varchar(100) COLLATE "default" NOT NULL,
"sort" numeric(10) NOT NULL,
"create_by" varchar(64) COLLATE "default" NOT NULL,
"create_date" timestamp(6) NOT NULL,
"update_by" varchar(64) COLLATE "default" NOT NULL,
"update_date" timestamp(6) NOT NULL,
"remarks" varchar(255) COLLATE "default",
"del_flag" char(1) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."test_tree" IS '树结构表';

COMMENT ON COLUMN "public"."test_tree"."id" IS '编号';

COMMENT ON COLUMN "public"."test_tree"."parent_id" IS '父级编号';

COMMENT ON COLUMN "public"."test_tree"."parent_ids" IS '所有父级编号';

COMMENT ON COLUMN "public"."test_tree"."name" IS '名称';

COMMENT ON COLUMN "public"."test_tree"."sort" IS '排序';

COMMENT ON COLUMN "public"."test_tree"."create_by" IS '创建者';

COMMENT ON COLUMN "public"."test_tree"."create_date" IS '创建时间';

COMMENT ON COLUMN "public"."test_tree"."update_by" IS '更新者';

COMMENT ON COLUMN "public"."test_tree"."update_date" IS '更新时间';

COMMENT ON COLUMN "public"."test_tree"."remarks" IS '备注信息';

COMMENT ON COLUMN "public"."test_tree"."del_flag" IS '删除标记';;

-- ----------------------------
--  Records 
-- ----------------------------
INSERT INTO "public"."act_ge_bytearray" VALUES ('09d296f72629468baaab73d92638670b','1','test_audit.bpmn20.xml','1e158e1936024039982d77e53dc25385','<?xml version=''1.0'' encoding=''UTF-8''?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:activiti="http://activiti.org/bpmn" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC" xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" typeLanguage="http://www.w3.org/2001/XMLSchema" expressionLanguage="http://www.w3.org/1999/XPath" targetNamespace="http://www.activiti.org/test">
  <process id="test_audit" name="流程审批测试流程" isExecutable="true">
    <startEvent id="start" name="启动审批" activiti:initiator="apply" activiti:formKey="/oa/testAudit/form"/>
    <endEvent id="end" name="结束审批"/>
    <userTask id="modify" name="员工薪酬档级修改" activiti:assignee="${apply}"/>
    <userTask id="audit" name="薪酬主管初审" activiti:assignee="thinkgem"/>
    <exclusiveGateway id="sid-C28BB5F6-013D-4570-B432-61B380C1F46F"/>
    <userTask id="audit2" name="集团人力资源部部长审核" activiti:assignee="thinkgem"/>
    <exclusiveGateway id="sid-ED46FE41-A0FD-496D-86DC-2C97AF5735F0"/>
    <sequenceFlow id="sid-EF2F51BB-1D99-4F0B-ACF2-B6C1300A7D2B" sourceRef="audit2" targetRef="sid-ED46FE41-A0FD-496D-86DC-2C97AF5735F0"/>
    <userTask id="audit3" name="集团人力资源部分管领导审核" activiti:assignee="thinkgem"/>
    <exclusiveGateway id="sid-FE485B2D-9A23-4236-BD0D-D788CA6E30E4"/>
    <sequenceFlow id="sid-3DBCD661-5720-4480-8156-748BE0275FEF" sourceRef="audit3" targetRef="sid-FE485B2D-9A23-4236-BD0D-D788CA6E30E4"/>
    <userTask id="audit4" name="集团总经理审批" activiti:assignee="thinkgem"/>
    <exclusiveGateway id="sid-3F53B6BD-F8F3-496B-AC08-50630BD11477"/>
    <userTask id="apply_end" name="薪酬档级兑现" activiti:assignee="thinkgem"/>
    <sequenceFlow id="sid-02DB2AD9-1332-4198-AC8D-22A35169D15C" sourceRef="audit4" targetRef="sid-3F53B6BD-F8F3-496B-AC08-50630BD11477"/>
    <sequenceFlow id="sid-2AB7C01A-50EE-4AAC-8C8F-F6E1935B3DA7" sourceRef="audit" targetRef="sid-C28BB5F6-013D-4570-B432-61B380C1F46F"/>
    <sequenceFlow id="sid-36E50C8B-6C7C-4968-B02D-EBAA425BF4BE" sourceRef="start" targetRef="audit"/>
    <sequenceFlow id="sid-7D723190-1432-411D-A4A4-774225E54CD9" name="是" sourceRef="sid-3F53B6BD-F8F3-496B-AC08-50630BD11477" targetRef="apply_end">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${pass==1}]]></conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="sid-D44CAD43-0271-4920-A524-9B8533E52550" name="是" sourceRef="sid-FE485B2D-9A23-4236-BD0D-D788CA6E30E4" targetRef="audit4">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${pass==1}]]></conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="sid-53258502-43EE-4DE8-B1A4-DBD11922B8AF" name="否" sourceRef="sid-C28BB5F6-013D-4570-B432-61B380C1F46F" targetRef="modify">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${pass==0}]]></conditionExpression>
    </sequenceFlow>
    <exclusiveGateway id="sid-5FED02D6-C388-48C6-870E-097DB2131EA0"/>
    <sequenceFlow id="sid-163DBC60-DBC9-438B-971A-67738FB7715A" sourceRef="modify" targetRef="sid-5FED02D6-C388-48C6-870E-097DB2131EA0"/>
    <sequenceFlow id="sid-72258A41-203E-428C-B71D-CA3506252D73" name="是" sourceRef="sid-C28BB5F6-013D-4570-B432-61B380C1F46F" targetRef="audit2">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${pass==1}]]></conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="sid-8448EF4A-B62E-4899-ABC2-0E2DB2AE6838" name="重新申请" sourceRef="sid-5FED02D6-C388-48C6-870E-097DB2131EA0" targetRef="audit">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${pass==1}]]></conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="sid-A7589084-4623-4FEA-A774-00A70DDC1D20" name="是" sourceRef="sid-ED46FE41-A0FD-496D-86DC-2C97AF5735F0" targetRef="audit3">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${pass==1}]]></conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="sid-FA618636-3708-4D0C-8514-29A4BB8BC926" name="否" sourceRef="sid-ED46FE41-A0FD-496D-86DC-2C97AF5735F0" targetRef="modify">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${pass==0}]]></conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="sid-1525BFF4-3E9D-4D8A-BF80-1F63AFE16289" name="否" sourceRef="sid-FE485B2D-9A23-4236-BD0D-D788CA6E30E4" targetRef="modify">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${pass==0}]]></conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="sid-35CC8C6C-1067-4398-991C-CCF955115965" name="否" sourceRef="sid-3F53B6BD-F8F3-496B-AC08-50630BD11477" targetRef="modify">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${pass==0}]]></conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="sid-BDB0AAB2-7E50-4D35-80EE-CE0BECDD9F57" sourceRef="apply_end" targetRef="end"/>
    <sequenceFlow id="sid-44AFB9C1-4057-4C48-B1F2-1EC897A52CB7" name="销毁" sourceRef="sid-5FED02D6-C388-48C6-870E-097DB2131EA0" targetRef="end">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${pass==0}]]></conditionExpression>
    </sequenceFlow>
  </process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_test_audit">
    <bpmndi:BPMNPlane bpmnElement="test_audit" id="BPMNPlane_test_audit">
      <bpmndi:BPMNShape bpmnElement="start" id="BPMNShape_start">
        <omgdc:Bounds height="30.0" width="30.0" x="30.0" y="245.0"/>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="end" id="BPMNShape_end">
        <omgdc:Bounds height="28.0" width="28.0" x="975.0" y="356.0"/>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="modify" id="BPMNShape_modify">
        <omgdc:Bounds height="58.0" width="102.0" x="209.0" y="135.0"/>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="audit" id="BPMNShape_audit">
        <omgdc:Bounds height="57.0" width="96.0" x="105.0" y="231.5"/>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="sid-C28BB5F6-013D-4570-B432-61B380C1F46F" id="BPMNShape_sid-C28BB5F6-013D-4570-B432-61B380C1F46F">
        <omgdc:Bounds height="40.0" width="40.0" x="240.0" y="240.0"/>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="audit2" id="BPMNShape_audit2">
        <omgdc:Bounds height="80.0" width="100.0" x="210.0" y="330.0"/>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="sid-ED46FE41-A0FD-496D-86DC-2C97AF5735F0" id="BPMNShape_sid-ED46FE41-A0FD-496D-86DC-2C97AF5735F0">
        <omgdc:Bounds height="40.0" width="40.0" x="345.0" y="350.0"/>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="audit3" id="BPMNShape_audit3">
        <omgdc:Bounds height="80.0" width="100.0" x="420.0" y="330.0"/>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="sid-FE485B2D-9A23-4236-BD0D-D788CA6E30E4" id="BPMNShape_sid-FE485B2D-9A23-4236-BD0D-D788CA6E30E4">
        <omgdc:Bounds height="40.0" width="40.0" x="555.0" y="350.0"/>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="audit4" id="BPMNShape_audit4">
        <omgdc:Bounds height="80.0" width="100.0" x="630.0" y="330.0"/>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="sid-3F53B6BD-F8F3-496B-AC08-50630BD11477" id="BPMNShape_sid-3F53B6BD-F8F3-496B-AC08-50630BD11477">
        <omgdc:Bounds height="40.0" width="40.0" x="765.0" y="350.0"/>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="apply_end" id="BPMNShape_apply_end">
        <omgdc:Bounds height="80.0" width="100.0" x="840.0" y="330.0"/>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="sid-5FED02D6-C388-48C6-870E-097DB2131EA0" id="BPMNShape_sid-5FED02D6-C388-48C6-870E-097DB2131EA0">
        <omgdc:Bounds height="40.0" width="40.0" x="240.0" y="45.0"/>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge bpmnElement="sid-3DBCD661-5720-4480-8156-748BE0275FEF" id="BPMNEdge_sid-3DBCD661-5720-4480-8156-748BE0275FEF">
        <omgdi:waypoint x="520.0" y="370.0"/>
        <omgdi:waypoint x="555.0" y="370.0"/>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-44AFB9C1-4057-4C48-B1F2-1EC897A52CB7" id="BPMNEdge_sid-44AFB9C1-4057-4C48-B1F2-1EC897A52CB7">
        <omgdi:waypoint x="280.0" y="65.0"/>
        <omgdi:waypoint x="989.0" y="65.0"/>
        <omgdi:waypoint x="989.0" y="356.0"/>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-8448EF4A-B62E-4899-ABC2-0E2DB2AE6838" id="BPMNEdge_sid-8448EF4A-B62E-4899-ABC2-0E2DB2AE6838">
        <omgdi:waypoint x="240.0" y="65.0"/>
        <omgdi:waypoint x="153.0" y="65.0"/>
        <omgdi:waypoint x="153.0" y="231.5"/>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-A7589084-4623-4FEA-A774-00A70DDC1D20" id="BPMNEdge_sid-A7589084-4623-4FEA-A774-00A70DDC1D20">
        <omgdi:waypoint x="385.0" y="370.0"/>
        <omgdi:waypoint x="420.0" y="370.0"/>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-35CC8C6C-1067-4398-991C-CCF955115965" id="BPMNEdge_sid-35CC8C6C-1067-4398-991C-CCF955115965">
        <omgdi:waypoint x="785.0" y="350.0"/>
        <omgdi:waypoint x="785.0" y="164.0"/>
        <omgdi:waypoint x="311.0" y="164.0"/>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-EF2F51BB-1D99-4F0B-ACF2-B6C1300A7D2B" id="BPMNEdge_sid-EF2F51BB-1D99-4F0B-ACF2-B6C1300A7D2B">
        <omgdi:waypoint x="310.0" y="370.0"/>
        <omgdi:waypoint x="345.0" y="370.0"/>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-7D723190-1432-411D-A4A4-774225E54CD9" id="BPMNEdge_sid-7D723190-1432-411D-A4A4-774225E54CD9">
        <omgdi:waypoint x="805.0" y="370.0"/>
        <omgdi:waypoint x="840.0" y="370.0"/>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-2AB7C01A-50EE-4AAC-8C8F-F6E1935B3DA7" id="BPMNEdge_sid-2AB7C01A-50EE-4AAC-8C8F-F6E1935B3DA7">
        <omgdi:waypoint x="201.0" y="260.0"/>
        <omgdi:waypoint x="240.0" y="260.0"/>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-D44CAD43-0271-4920-A524-9B8533E52550" id="BPMNEdge_sid-D44CAD43-0271-4920-A524-9B8533E52550">
        <omgdi:waypoint x="595.0" y="370.0"/>
        <omgdi:waypoint x="630.0" y="370.0"/>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-FA618636-3708-4D0C-8514-29A4BB8BC926" id="BPMNEdge_sid-FA618636-3708-4D0C-8514-29A4BB8BC926">
        <omgdi:waypoint x="365.0" y="350.0"/>
        <omgdi:waypoint x="365.0" y="164.0"/>
        <omgdi:waypoint x="311.0" y="164.0"/>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-02DB2AD9-1332-4198-AC8D-22A35169D15C" id="BPMNEdge_sid-02DB2AD9-1332-4198-AC8D-22A35169D15C">
        <omgdi:waypoint x="730.0" y="370.0"/>
        <omgdi:waypoint x="765.0" y="370.0"/>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-36E50C8B-6C7C-4968-B02D-EBAA425BF4BE" id="BPMNEdge_sid-36E50C8B-6C7C-4968-B02D-EBAA425BF4BE">
        <omgdi:waypoint x="60.0" y="260.0"/>
        <omgdi:waypoint x="105.0" y="260.0"/>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-53258502-43EE-4DE8-B1A4-DBD11922B8AF" id="BPMNEdge_sid-53258502-43EE-4DE8-B1A4-DBD11922B8AF">
        <omgdi:waypoint x="260.0" y="240.0"/>
        <omgdi:waypoint x="260.0" y="193.0"/>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-163DBC60-DBC9-438B-971A-67738FB7715A" id="BPMNEdge_sid-163DBC60-DBC9-438B-971A-67738FB7715A">
        <omgdi:waypoint x="260.0" y="135.0"/>
        <omgdi:waypoint x="260.0" y="85.0"/>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-BDB0AAB2-7E50-4D35-80EE-CE0BECDD9F57" id="BPMNEdge_sid-BDB0AAB2-7E50-4D35-80EE-CE0BECDD9F57">
        <omgdi:waypoint x="940.0" y="370.0"/>
        <omgdi:waypoint x="975.0" y="370.0"/>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-1525BFF4-3E9D-4D8A-BF80-1F63AFE16289" id="BPMNEdge_sid-1525BFF4-3E9D-4D8A-BF80-1F63AFE16289">
        <omgdi:waypoint x="575.0" y="350.0"/>
        <omgdi:waypoint x="575.0" y="164.0"/>
        <omgdi:waypoint x="311.0" y="164.0"/>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-72258A41-203E-428C-B71D-CA3506252D73" id="BPMNEdge_sid-72258A41-203E-428C-B71D-CA3506252D73">
        <omgdi:waypoint x="260.0" y="280.0"/>
        <omgdi:waypoint x="260.0" y="330.0"/>
      </bpmndi:BPMNEdge>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</definitions>','f'); INSERT INTO "public"."act_ge_bytearray" VALUES ('a66e83edf1ac4e42826450bf6ec4b5cb','1','test_audit.png','1e158e1936024039982d77e53dc25385','�PNG

','f');
INSERT INTO "public"."act_ge_property" VALUES ('next.dbid','1','1'); INSERT INTO "public"."act_ge_property" VALUES ('schema.history','create(5.21.0.0)','1'); INSERT INTO "public"."act_ge_property" VALUES ('schema.version','5.21.0.0','1');
INSERT INTO "public"."act_re_deployment" VALUES ('1e158e1936024039982d77e53dc25385','SpringAutoDeployment',NULL,'','2016-09-09 13:33:02.781');
INSERT INTO "public"."act_re_procdef" VALUES ('test_audit:1:1f27fa49cf4a4c2ba27bfe7cfc7924b4','1','http://www.activiti.org/test','流程审批测试流程','test_audit','1','1e158e1936024039982d77e53dc25385','test_audit.bpmn20.xml','test_audit.png',NULL,'t','t','1','');
INSERT INTO "public"."cms_category" VALUES ('1','0','0,','0','1',NULL,'顶级栏目',NULL,NULL,NULL,NULL,NULL,'0','1','1','0','0','1',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('10','1','0,1,','1','4','article','软件介绍',NULL,NULL,NULL,NULL,NULL,'20','1','1','0','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('11','10','0,1,10,','1','4','article','网络工具',NULL,NULL,NULL,NULL,NULL,'30','1','1','0','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('12','10','0,1,10,','1','4','article','浏览工具',NULL,NULL,NULL,NULL,NULL,'40','1','1','0','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('13','10','0,1,10,','1','4','article','浏览辅助',NULL,NULL,NULL,NULL,NULL,'50','1','1','0','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('14','10','0,1,10,','1','4','article','网络优化',NULL,NULL,NULL,NULL,NULL,'50','1','1','0','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('15','10','0,1,10,','1','4','article','邮件处理',NULL,NULL,NULL,NULL,NULL,'50','1','1','0','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('16','10','0,1,10,','1','4','article','下载工具',NULL,NULL,NULL,NULL,NULL,'50','1','1','0','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('17','10','0,1,10,','1','4','article','搜索工具',NULL,NULL,NULL,NULL,NULL,'50','1','1','2','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('18','1','0,1,','1','5','link','友情链接',NULL,NULL,NULL,NULL,NULL,'90','1','1','0','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('19','18','0,1,18,','1','5','link','常用网站',NULL,NULL,NULL,NULL,NULL,'50','1','1','0','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('2','1','0,1,','1','3','article','组织机构',NULL,NULL,NULL,NULL,NULL,'10','1','1','0','0','1',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('20','18','0,1,18,','1','5','link','门户网站',NULL,NULL,NULL,NULL,NULL,'50','1','1','0','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('21','18','0,1,18,','1','5','link','购物网站',NULL,NULL,NULL,NULL,NULL,'50','1','1','0','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('22','18','0,1,18,','1','5','link','交友社区',NULL,NULL,NULL,NULL,NULL,'50','1','1','0','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('23','18','0,1,18,','1','5','link','音乐视频',NULL,NULL,NULL,NULL,NULL,'50','1','1','0','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('24','1','0,1,','1','6',NULL,'百度一下',NULL,'http://www.baidu.com','_blank',NULL,NULL,'90','1','1','0','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('25','1','0,1,','1','6',NULL,'全文检索',NULL,'/search',NULL,NULL,NULL,'90','0','1','0','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('26','1','0,1,','2','6','article','测试栏目',NULL,NULL,NULL,NULL,NULL,'90','1','1','0','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('27','1','0,1,','1','6',NULL,'公共留言',NULL,'/guestbook',NULL,NULL,NULL,'90','1','1','0','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('3','2','0,1,2,','1','3','article','网站简介',NULL,NULL,NULL,NULL,NULL,'30','1','1','0','0','1',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('4','2','0,1,2,','1','3','article','内部机构',NULL,NULL,NULL,NULL,NULL,'40','1','1','0','0','1',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('5','2','0,1,2,','1','3','article','地方机构',NULL,NULL,NULL,NULL,NULL,'50','1','1','0','0','1',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('6','1','0,1,','1','3','article','质量检验',NULL,NULL,NULL,NULL,NULL,'20','1','1','1','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('7','6','0,1,6,','1','3','article','产品质量',NULL,NULL,NULL,NULL,NULL,'30','1','1','0','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('8','6','0,1,6,','1','3','article','技术质量',NULL,NULL,NULL,NULL,NULL,'40','1','1','0','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_category" VALUES ('9','6','0,1,6,','1','3','article','工程质量',NULL,NULL,NULL,NULL,NULL,'50','1','1','0','1','0',NULL,NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0');
INSERT INTO "public"."cms_link" VALUES ('1','19','JeeSite',NULL,NULL,'http://thinkgem.github.com/jeesite','0',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_link" VALUES ('10','22','58同城',NULL,NULL,'http://www.58.com/','0',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_link" VALUES ('11','23','视频大全',NULL,NULL,'http://v.360.cn/','0',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_link" VALUES ('12','23','凤凰网',NULL,NULL,'http://www.ifeng.com/','0',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_link" VALUES ('2','19','ThinkGem',NULL,NULL,'http://thinkgem.iteye.com/','0',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_link" VALUES ('3','19','百度一下',NULL,NULL,'http://www.baidu.com','0',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_link" VALUES ('4','19','谷歌搜索',NULL,NULL,'http://www.google.com','0',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_link" VALUES ('5','20','新浪网',NULL,NULL,'http://www.sina.com.cn','0',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_link" VALUES ('6','20','腾讯网',NULL,NULL,'http://www.qq.com/','0',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_link" VALUES ('7','21','淘宝网',NULL,NULL,'http://www.taobao.com/','0',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_link" VALUES ('8','21','新华网',NULL,NULL,'http://www.xinhuanet.com/','0',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."cms_link" VALUES ('9','22','赶集网',NULL,NULL,'http://www.ganji.com/','0',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0');
INSERT INTO "public"."cms_site" VALUES ('1','默认站点','Web','','','Web','JeeSite','basic','','','1','2013-05-27 00:00:00','1','2013-05-27 00:00:00','','0'); INSERT INTO "public"."cms_site" VALUES ('2','子站点测试','Subsite','','','subsite','JeeSite subsite','basic','','','1','2013-05-27 00:00:00','1','2013-05-27 00:00:00','','0');
INSERT INTO "public"."gen_scheme" VALUES ('35a13dc260284a728a270db3f382664b','树结构','treeTable','com.thinkgem.jeesite.modules','test',NULL,'树结构生成','树结构','ThinkGem','f6e4dafaa72f4c509636484715f33a96','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_scheme" VALUES ('9c9de9db6da743bb899036c6546061ac','单表','curd','com.thinkgem.jeesite.modules','test',NULL,'单表生成','单表','ThinkGem','aef6f1fc948f4c9ab1c1b780bc471cc2','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_scheme" VALUES ('e6d905fd236b46d1af581dd32bdfb3b0','主子表','curd_many','com.thinkgem.jeesite.modules','test',NULL,'主子表生成','主子表','ThinkGem','43d6d5acffa14c258340ce6765e46c6f','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0');
INSERT INTO "public"."gen_table" VALUES ('43d6d5acffa14c258340ce6765e46c6f','test_data_main','业务数据表','TestDataMain',NULL,NULL,'1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table" VALUES ('6e05c389f3c6415ea34e55e9dfb28934','test_data_child','业务数据子表','TestDataChild','test_data_main','test_data_main_id','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table" VALUES ('aef6f1fc948f4c9ab1c1b780bc471cc2','test_data','业务数据表','TestData',NULL,NULL,'1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table" VALUES ('f6e4dafaa72f4c509636484715f33a96','test_tree','树结构表','TestTree',NULL,NULL,'1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0');
INSERT INTO "public"."gen_table_column" VALUES ('0902a0cb3e8f434280c20e9d771d0658','aef6f1fc948f4c9ab1c1b780bc471cc2','sex','性别','char(1)','String','sex','0','1','1','1','1','1','=','radiobox','sex',NULL,'6','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('103fc05c88ff40639875c2111881996a','aef6f1fc948f4c9ab1c1b780bc471cc2','create_date','创建时间','timestamp(6)','java.util.Date','createDate','0','0','1','0','0','0','=','dateselect',NULL,NULL,'9','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('12fa38dd986e41908f7fefa5839d1220','6e05c389f3c6415ea34e55e9dfb28934','create_by','创建者','varchar2(64)','com.thinkgem.jeesite.modules.modules.sys.entity.User','createBy.id','0','0','1','0','0','0','=','input',NULL,NULL,'4','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('195ee9241f954d008fe01625f4adbfef','f6e4dafaa72f4c509636484715f33a96','create_by','创建者','varchar2(64)','com.thinkgem.jeesite.modules.modules.sys.entity.User','createBy.id','0','0','1','0','0','0','=','input',NULL,NULL,'6','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('19c6478b8ff54c60910c2e4fc3d27503','43d6d5acffa14c258340ce6765e46c6f','id','编号','varchar2(64)','String','id','1','0','1','0','0','0','=','input',NULL,NULL,'1','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('1ac6562f753d4e599693840651ab2bf7','43d6d5acffa14c258340ce6765e46c6f','in_date','加入日期','date(7)','java.util.Date','inDate','0','1','1','1','0','0','=','dateselect',NULL,NULL,'7','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('1b8eb55f65284fa6b0a5879b6d8ad3ec','aef6f1fc948f4c9ab1c1b780bc471cc2','in_date','加入日期','date(7)','java.util.Date','inDate','0','1','1','1','0','1','between','dateselect',NULL,NULL,'7','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('1d5ca4d114be41e99f8dc42a682ba609','aef6f1fc948f4c9ab1c1b780bc471cc2','user_id','归属用户','varchar2(64)','com.thinkgem.jeesite.modules.modules.sys.entity.User','user.id|name','0','1','1','1','1','1','=','userselect',NULL,NULL,'2','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('21756504ffdc487eb167a823f89c0c06','43d6d5acffa14c258340ce6765e46c6f','update_by','更新者','varchar2(64)','com.thinkgem.jeesite.modules.modules.sys.entity.User','updateBy.id','0','0','1','1','0','0','=','input',NULL,NULL,'10','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('24bbdc0a555e4412a106ab1c5f03008e','f6e4dafaa72f4c509636484715f33a96','parent_ids','所有父级编号','varchar2(2000)','String','parentIds','0','0','1','1','0','0','like','input',NULL,NULL,'3','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('33152ce420904594b3eac796a27f0560','6e05c389f3c6415ea34e55e9dfb28934','id','编号','varchar2(64)','String','id','1','0','1','0','0','0','=','input',NULL,NULL,'1','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('35af241859624a01917ab64c3f4f0813','aef6f1fc948f4c9ab1c1b780bc471cc2','del_flag','删除标记（0：正常；1：删除）','char(1)','String','delFlag','0','0','1','0','0','0','=','radiobox','del_flag',NULL,'13','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('398b4a03f06940bfb979ca574e1911e3','aef6f1fc948f4c9ab1c1b780bc471cc2','create_by','创建者','varchar2(64)','com.thinkgem.jeesite.modules.modules.sys.entity.User','createBy.id','0','0','1','0','0','0','=','input',NULL,NULL,'8','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('3a7cf23ae48a4c849ceb03feffc7a524','43d6d5acffa14c258340ce6765e46c6f','area_id','归属区域','nvarchar2(64)','com.thinkgem.jeesite.modules.modules.sys.entity.Area','area.id|name','0','1','1','1','0','0','=','areaselect',NULL,NULL,'4','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('3d9c32865bb44e85af73381df0ffbf3d','43d6d5acffa14c258340ce6765e46c6f','update_date','更新时间','timestamp(6)','java.util.Date','updateDate','0','0','1','1','1','0','=','dateselect',NULL,NULL,'11','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('416c76d2019b4f76a96d8dc3a8faf84c','f6e4dafaa72f4c509636484715f33a96','update_date','更新时间','timestamp(6)','java.util.Date','updateDate','0','0','1','1','1','0','=','dateselect',NULL,NULL,'9','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('46e6d8283270493687085d29efdecb05','f6e4dafaa72f4c509636484715f33a96','del_flag','删除标记（0：正常；1：删除）','char(1)','String','delFlag','0','0','1','0','0','0','=','radiobox','del_flag',NULL,'11','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('4a0a1fff86ca46519477d66b82e01991','aef6f1fc948f4c9ab1c1b780bc471cc2','name','名称','nvarchar2(100)','String','name','0','1','1','1','1','1','like','input',NULL,NULL,'5','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('4c8ef12cb6924b9ba44048ba9913150b','43d6d5acffa14c258340ce6765e46c6f','create_date','创建时间','timestamp(6)','java.util.Date','createDate','0','0','1','0','0','0','=','dateselect',NULL,NULL,'9','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('53d65a3d306d4fac9e561db9d3c66912','6e05c389f3c6415ea34e55e9dfb28934','del_flag','删除标记（0：正常；1：删除）','char(1)','String','delFlag','0','0','1','0','0','0','=','radiobox','del_flag',NULL,'9','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('56fa71c0bd7e4132931874e548dc9ba5','6e05c389f3c6415ea34e55e9dfb28934','update_by','更新者','varchar2(64)','com.thinkgem.jeesite.modules.modules.sys.entity.User','updateBy.id','0','0','1','1','0','0','=','input',NULL,NULL,'6','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('5a4a1933c9c844fdba99de043dc8205e','aef6f1fc948f4c9ab1c1b780bc471cc2','update_by','更新者','varchar2(64)','com.thinkgem.jeesite.modules.modules.sys.entity.User','updateBy.id','0','0','1','1','0','0','=','input',NULL,NULL,'10','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('5e5c69bd3eaa4dcc9743f361f3771c08','aef6f1fc948f4c9ab1c1b780bc471cc2','id','编号','varchar2(64)','String','id','1','0','1','0','0','0','=','input',NULL,NULL,'1','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('633f5a49ec974c099158e7b3e6bfa930','f6e4dafaa72f4c509636484715f33a96','name','名称','nvarchar2(100)','String','name','0','0','1','1','1','1','like','input',NULL,NULL,'4','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('652491500f2641ffa7caf95a93e64d34','6e05c389f3c6415ea34e55e9dfb28934','update_date','更新时间','timestamp(6)','java.util.Date','updateDate','0','0','1','1','1','0','=','dateselect',NULL,NULL,'7','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('6763ff6dc7cd4c668e76cf9b697d3ff6','f6e4dafaa72f4c509636484715f33a96','sort','排序','number(10)','Integer','sort','0','0','1','1','1','0','=','input',NULL,NULL,'5','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('67d0331f809a48ee825602659f0778e8','43d6d5acffa14c258340ce6765e46c6f','name','名称','nvarchar2(100)','String','name','0','1','1','1','1','1','like','input',NULL,NULL,'5','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('68345713bef3445c906f70e68f55de38','6e05c389f3c6415ea34e55e9dfb28934','test_data_main_id','业务主表','varchar2(64)','String','testDataMain.id','0','1','1','1','0','0','=','input',NULL,NULL,'2','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('71ea4bc10d274911b405f3165fc1bb1a','aef6f1fc948f4c9ab1c1b780bc471cc2','area_id','归属区域','nvarchar2(64)','com.thinkgem.jeesite.modules.modules.sys.entity.Area','area.id|name','0','1','1','1','1','1','=','areaselect',NULL,NULL,'4','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('7f871058d94c4d9a89084be7c9ce806d','6e05c389f3c6415ea34e55e9dfb28934','remarks','备注信息','nvarchar2(255)','String','remarks','0','1','1','1','1','0','=','input',NULL,NULL,'8','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('8b48774cfe184913b8b5eb17639cf12d','43d6d5acffa14c258340ce6765e46c6f','create_by','创建者','varchar2(64)','com.thinkgem.jeesite.modules.modules.sys.entity.User','createBy.id','0','0','1','0','0','0','=','input',NULL,NULL,'8','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('8b7cf0525519474ebe1de9e587eb7067','6e05c389f3c6415ea34e55e9dfb28934','create_date','创建时间','timestamp(6)','java.util.Date','createDate','0','0','1','0','0','0','=','dateselect',NULL,NULL,'5','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('8b9de88df53e485d8ef461c4b1824bc1','43d6d5acffa14c258340ce6765e46c6f','user_id','归属用户','varchar2(64)','com.thinkgem.jeesite.modules.modules.sys.entity.User','user.id|name','0','1','1','1','1','1','=','userselect',NULL,NULL,'2','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('8da38dbe5fe54e9bb1f9682c27fbf403','aef6f1fc948f4c9ab1c1b780bc471cc2','remarks','备注信息','nvarchar2(255)','String','remarks','0','1','1','1','1','0','=','textarea',NULL,NULL,'12','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('92481c16a0b94b0e8bba16c3c54eb1e4','f6e4dafaa72f4c509636484715f33a96','create_date','创建时间','timestamp(6)','java.util.Date','createDate','0','0','1','0','0','0','=','dateselect',NULL,NULL,'7','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('9a012c1d2f934dbf996679adb7cc827a','f6e4dafaa72f4c509636484715f33a96','parent_id','父级编号','varchar2(64)','This','parent.id|name','0','0','1','1','0','0','=','treeselect',NULL,NULL,'2','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('ad3bf0d4b44b4528a5211a66af88f322','aef6f1fc948f4c9ab1c1b780bc471cc2','office_id','归属部门','varchar2(64)','com.thinkgem.jeesite.modules.modules.sys.entity.Office','office.id|name','0','1','1','1','1','1','=','officeselect',NULL,NULL,'3','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('bb1256a8d1b741f6936d8fed06f45eed','f6e4dafaa72f4c509636484715f33a96','update_by','更新者','varchar2(64)','com.thinkgem.jeesite.modules.modules.sys.entity.User','updateBy.id','0','0','1','1','0','0','=','input',NULL,NULL,'8','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('ca68a2d403f0449cbaa1d54198c6f350','43d6d5acffa14c258340ce6765e46c6f','office_id','归属部门','varchar2(64)','com.thinkgem.jeesite.modules.modules.sys.entity.Office','office.id|name','0','1','1','1','0','0','=','officeselect',NULL,NULL,'3','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('cb9c0ec3da26432d9cbac05ede0fd1d0','43d6d5acffa14c258340ce6765e46c6f','remarks','备注信息','nvarchar2(255)','String','remarks','0','1','1','1','1','0','=','textarea',NULL,NULL,'12','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('cfcfa06ea61749c9b4c4dbc507e0e580','f6e4dafaa72f4c509636484715f33a96','id','编号','varchar2(64)','String','id','1','0','1','0','0','0','=','input',NULL,NULL,'1','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('d5c2d932ae904aa8a9f9ef34cd36fb0b','43d6d5acffa14c258340ce6765e46c6f','sex','性别','char(1)','String','sex','0','1','1','1','0','1','=','select','sex',NULL,'6','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('e64050a2ebf041faa16f12dda5dcf784','6e05c389f3c6415ea34e55e9dfb28934','name','名称','nvarchar2(100)','String','name','0','1','1','1','1','1','like','input',NULL,NULL,'3','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('e8d11127952d4aa288bb3901fc83127f','43d6d5acffa14c258340ce6765e46c6f','del_flag','删除标记（0：正常；1：删除）','char(1)','String','delFlag','0','0','1','0','0','0','=','radiobox','del_flag',NULL,'13','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('eb2e5afd13f147a990d30e68e7f64e12','aef6f1fc948f4c9ab1c1b780bc471cc2','update_date','更新时间','timestamp(6)','java.util.Date','updateDate','0','0','1','1','1','0','=','dateselect',NULL,NULL,'11','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0'); INSERT INTO "public"."gen_table_column" VALUES ('f5ed8c82bad0413fbfcccefa95931358','f6e4dafaa72f4c509636484715f33a96','remarks','备注信息','nvarchar2(255)','String','remarks','0','1','1','1','1','0','=','textarea',NULL,NULL,'10','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05',NULL,'0');
INSERT INTO "public"."sys_area" VALUES ('1','0','0,','中国','10','100000','1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_area" VALUES ('2','1','0,1,','山东省','20','110000','2','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_area" VALUES ('3','2','0,1,2,','济南市','30','110101','3','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_area" VALUES ('4','3','0,1,2,3,','历城区','40','110102','4','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_area" VALUES ('5','3','0,1,2,3,','历下区','50','110104','4','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_area" VALUES ('6','3','0,1,2,3,','高新区','60','110105','4','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0');
INSERT INTO "public"."sys_dict" VALUES ('1','0','正常','del_flag','删除标记','10','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('10','yellow','黄色','color','颜色值','40','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('100','java.util.Date','Date','gen_java_type','Java类型','50','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('101','com.thinkgem.jeesite.modules.sys.entity.User','User','gen_java_type','Java类型','60','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('102','com.thinkgem.jeesite.modules.sys.entity.Office','Office','gen_java_type','Java类型','70','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('103','com.thinkgem.jeesite.modules.sys.entity.Area','Area','gen_java_type','Java类型','80','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('104','Custom','Custom','gen_java_type','Java类型','90','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('105','1','会议通告','oa_notify_type','通知通告类型','10','0','1','2013-11-08 08:00:00','1','2013-11-08 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('106','2','奖惩通告','oa_notify_type','通知通告类型','20','0','1','2013-11-08 08:00:00','1','2013-11-08 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('107','3','活动通告','oa_notify_type','通知通告类型','30','0','1','2013-11-08 08:00:00','1','2013-11-08 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('108','0','草稿','oa_notify_status','通知通告状态','10','0','1','2013-11-08 08:00:00','1','2013-11-08 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('109','1','发布','oa_notify_status','通知通告状态','20','0','1','2013-11-08 08:00:00','1','2013-11-08 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('11','orange','橙色','color','颜色值','50','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('110','0','未读','oa_notify_read','通知通告状态','10','0','1','2013-11-08 08:00:00','1','2013-11-08 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('111','1','已读','oa_notify_read','通知通告状态','20','0','1','2013-11-08 08:00:00','1','2013-11-08 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('12','default','默认主题','theme','主题方案','10','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('13','cerulean','天蓝主题','theme','主题方案','20','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('14','readable','橙色主题','theme','主题方案','30','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('15','united','红色主题','theme','主题方案','40','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('16','flat','Flat主题','theme','主题方案','60','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('17','1','国家','sys_area_type','区域类型','10','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('18','2','省份、直辖市','sys_area_type','区域类型','20','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('19','3','地市','sys_area_type','区域类型','30','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('2','1','删除','del_flag','删除标记','20','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('20','4','区县','sys_area_type','区域类型','40','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('21','1','公司','sys_office_type','机构类型','60','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('22','2','部门','sys_office_type','机构类型','70','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('23','3','小组','sys_office_type','机构类型','80','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('24','4','其它','sys_office_type','机构类型','90','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('25','1','综合部','sys_office_common','快捷通用部门','30','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('26','2','开发部','sys_office_common','快捷通用部门','40','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('27','3','人力部','sys_office_common','快捷通用部门','50','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('28','1','一级','sys_office_grade','机构等级','10','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('29','2','二级','sys_office_grade','机构等级','20','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('3','1','显示','show_hide','显示/隐藏','10','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('30','3','三级','sys_office_grade','机构等级','30','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('31','4','四级','sys_office_grade','机构等级','40','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('32','1','所有数据','sys_data_scope','数据范围','10','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('33','2','所在公司及以下数据','sys_data_scope','数据范围','20','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('34','3','所在公司数据','sys_data_scope','数据范围','30','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('35','4','所在部门及以下数据','sys_data_scope','数据范围','40','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('36','5','所在部门数据','sys_data_scope','数据范围','50','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('37','8','仅本人数据','sys_data_scope','数据范围','90','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('38','9','按明细设置','sys_data_scope','数据范围','100','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('39','1','系统管理','sys_user_type','用户类型','10','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('4','0','隐藏','show_hide','显示/隐藏','20','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('40','2','部门经理','sys_user_type','用户类型','20','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('41','3','普通用户','sys_user_type','用户类型','30','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('42','basic','基础主题','cms_theme','站点主题','10','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('43','blue','蓝色主题','cms_theme','站点主题','20','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('44','red','红色主题','cms_theme','站点主题','30','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('45','article','文章模型','cms_module','栏目模型','10','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('46','picture','图片模型','cms_module','栏目模型','20','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('47','download','下载模型','cms_module','栏目模型','30','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('48','link','链接模型','cms_module','栏目模型','40','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('49','special','专题模型','cms_module','栏目模型','50','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('5','1','是','yes_no','是/否','10','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('50','0','默认展现方式','cms_show_modes','展现方式','10','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('51','1','首栏目内容列表','cms_show_modes','展现方式','20','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('52','2','栏目第一条内容','cms_show_modes','展现方式','30','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('53','0','发布','cms_del_flag','内容状态','10','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('54','1','删除','cms_del_flag','内容状态','20','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('55','2','审核','cms_del_flag','内容状态','15','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('56','1','首页焦点图','cms_posid','推荐位','10','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('57','2','栏目页文章推荐','cms_posid','推荐位','20','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('58','1','咨询','cms_guestbook','留言板分类','10','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('59','2','建议','cms_guestbook','留言板分类','20','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('6','0','否','yes_no','是/否','20','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('60','3','投诉','cms_guestbook','留言板分类','30','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('61','4','其它','cms_guestbook','留言板分类','40','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('62','1','公休','oa_leave_type','请假类型','10','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('63','2','病假','oa_leave_type','请假类型','20','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('64','3','事假','oa_leave_type','请假类型','30','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('65','4','调休','oa_leave_type','请假类型','40','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('66','5','婚假','oa_leave_type','请假类型','60','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('67','1','接入日志','sys_log_type','日志类型','30','0','1','2013-06-03 08:00:00','1','2013-06-03 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('68','2','异常日志','sys_log_type','日志类型','40','0','1','2013-06-03 08:00:00','1','2013-06-03 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('69','leave','请假流程','act_type','流程类型','10','0','1','2013-06-03 08:00:00','1','2013-06-03 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('7','red','红色','color','颜色值','10','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('70','test_audit','审批测试流程','act_type','流程类型','20','0','1','2013-06-03 08:00:00','1','2013-06-03 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('71','1','分类1','act_category','流程分类','10','0','1','2013-06-03 08:00:00','1','2013-06-03 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('72','2','分类2','act_category','流程分类','20','0','1','2013-06-03 08:00:00','1','2013-06-03 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('73','crud','增删改查','gen_category','代码生成分类','10','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('74','crud_many','增删改查（包含从表）','gen_category','代码生成分类','20','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('75','tree','树结构','gen_category','代码生成分类','30','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('76','=','=','gen_query_type','查询方式','10','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('77','!=','!=','gen_query_type','查询方式','20','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('78','&gt;','&gt;','gen_query_type','查询方式','30','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('79','&lt;','&lt;','gen_query_type','查询方式','40','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('8','green','绿色','color','颜色值','20','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('80','between','Between','gen_query_type','查询方式','50','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('81','like','Like','gen_query_type','查询方式','60','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('82','left_like','Left Like','gen_query_type','查询方式','70','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('83','right_like','Right Like','gen_query_type','查询方式','80','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('84','input','文本框','gen_show_type','字段生成方案','10','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('85','textarea','文本域','gen_show_type','字段生成方案','20','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('86','select','下拉框','gen_show_type','字段生成方案','30','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('87','checkbox','复选框','gen_show_type','字段生成方案','40','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('88','radiobox','单选框','gen_show_type','字段生成方案','50','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('89','dateselect','日期选择','gen_show_type','字段生成方案','60','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1');
INSERT INTO "public"."sys_dict" VALUES ('9','blue','蓝色','color','颜色值','30','0','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('90','userselect','人员选择','gen_show_type','字段生成方案','70','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('91','officeselect','部门选择','gen_show_type','字段生成方案','80','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('92','areaselect','区域选择','gen_show_type','字段生成方案','90','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('93','String','String','gen_java_type','Java类型','10','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('94','Long','Long','gen_java_type','Java类型','20','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('95','dao','仅持久层','gen_category','代码生成分类','40','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('96','1','男','sex','性别','10','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('97','2','女','sex','性别','20','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'0'); INSERT INTO "public"."sys_dict" VALUES ('98','Integer','Integer','gen_java_type','Java类型','30','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1'); INSERT INTO "public"."sys_dict" VALUES ('99','Double','Double','gen_java_type','Java类型','40','0','1','2013-10-28 08:00:00','1','2013-10-28 08:00:00',NULL,'1');
INSERT INTO "public"."sys_menu" VALUES ('0b2ebd4d639e4c2b83c2dd0764522f24','ba8092291b40482db8fe7fc006ea3d76','0,1,79,3c92c17886944d0687e73e286cada573,ba8092291b40482db8fe7fc006ea3d76,','编辑','60','','','','0','test:testData:edit','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05','','0'); INSERT INTO "public"."sys_menu" VALUES ('0ca004d6b1bf4bcab9670a5060d82a55','3c92c17886944d0687e73e286cada573','0,1,79,3c92c17886944d0687e73e286cada573,','树结构','90','/test/testTree','','','1','','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05','','0'); INSERT INTO "public"."sys_menu" VALUES ('1','0','0,','功能菜单','0',NULL,NULL,NULL,'1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('10','3','0,1,2,3,','字典管理','60','/sys/dict/',NULL,'th-list','1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('11','10','0,1,2,3,10,','查看','30',NULL,NULL,NULL,'0','sys:dict:view','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('12','10','0,1,2,3,10,','修改','40',NULL,NULL,NULL,'0','sys:dict:edit','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('13','2','0,1,2,','机构用户','970',NULL,NULL,NULL,'1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('14','13','0,1,2,13,','区域管理','50','/sys/area/',NULL,'th','1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('15','14','0,1,2,13,14,','查看','30',NULL,NULL,NULL,'0','sys:area:view','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('16','14','0,1,2,13,14,','修改','40',NULL,NULL,NULL,'0','sys:area:edit','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('17','13','0,1,2,13,','机构管理','40','/sys/office/',NULL,'th-large','1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('18','17','0,1,2,13,17,','查看','30',NULL,NULL,NULL,'0','sys:office:view','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('19','17','0,1,2,13,17,','修改','40',NULL,NULL,NULL,'0','sys:office:edit','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('2','1','0,1,','系统设置','900',NULL,NULL,NULL,'1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('20','13','0,1,2,13,','用户管理','30','/sys/user/index',NULL,'user','1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('21','20','0,1,2,13,20,','查看','30',NULL,NULL,NULL,'0','sys:user:view','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('22','20','0,1,2,13,20,','修改','40',NULL,NULL,NULL,'0','sys:user:edit','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('27','1','0,1,','我的面板','100',NULL,NULL,NULL,'1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('28','27','0,1,27,','个人信息','30',NULL,NULL,NULL,'1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('29','28','0,1,27,28,','个人信息','30','/sys/user/info',NULL,'user','1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('3','2','0,1,2,','系统设置','980',NULL,NULL,NULL,'1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('30','28','0,1,27,28,','修改密码','40','/sys/user/modifyPwd',NULL,'lock','1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('31','1','0,1,','内容管理','500',NULL,NULL,NULL,'1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('32','31','0,1,31,','栏目设置','990',NULL,NULL,NULL,'1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('33','32','0,1,31,32','栏目管理','30','/cms/category/',NULL,'align-justify','1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('34','33','0,1,31,32,33,','查看','30',NULL,NULL,NULL,'0','cms:category:view','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('35','33','0,1,31,32,33,','修改','40',NULL,NULL,NULL,'0','cms:category:edit','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('36','32','0,1,31,32','站点设置','40','/cms/site/',NULL,'certificate','1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('37','36','0,1,31,32,36,','查看','30',NULL,NULL,NULL,'0','cms:site:view','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('38','36','0,1,31,32,36,','修改','40',NULL,NULL,NULL,'0','cms:site:edit','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('39','32','0,1,31,32','切换站点','50','/cms/site/select',NULL,'retweet','1','cms:site:select','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('3c92c17886944d0687e73e286cada573','79','0,1,79,','生成示例','120','','','','1','','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05','','0'); INSERT INTO "public"."sys_menu" VALUES ('4','3','0,1,2,3,','菜单管理','30','/sys/menu/',NULL,'list-alt','1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('40','31','0,1,31,','内容管理','500',NULL,NULL,NULL,'1','cms:view','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('41','40','0,1,31,40,','内容发布','30','/cms/',NULL,'briefcase','1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('42','41','0,1,31,40,41,','文章模型','40','/cms/article/',NULL,'file','0',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('43','42','0,1,31,40,41,42,','查看','30',NULL,NULL,NULL,'0','cms:article:view','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('44','42','0,1,31,40,41,42,','修改','40',NULL,NULL,NULL,'0','cms:article:edit','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('45','42','0,1,31,40,41,42,','审核','50',NULL,NULL,NULL,'0','cms:article:audit','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('46','41','0,1,31,40,41,','链接模型','60','/cms/link/',NULL,'random','0',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('47','46','0,1,31,40,41,46,','查看','30',NULL,NULL,NULL,'0','cms:link:view','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('48','46','0,1,31,40,41,46,','修改','40',NULL,NULL,NULL,'0','cms:link:edit','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('4855cf3b25c244fb8500a380db189d97','b1f6d1b86ba24365bae7fd86c5082317','0,1,79,3c92c17886944d0687e73e286cada573,b1f6d1b86ba24365bae7fd86c5082317,','查看','30','','','','0','test:testDataMain:view','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05','','0'); INSERT INTO "public"."sys_menu" VALUES ('49','46','0,1,31,40,41,46,','审核','50',NULL,NULL,NULL,'0','cms:link:audit','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('5','4','0,1,2,3,4,','查看','30',NULL,NULL,NULL,'0','sys:menu:view','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('50','40','0,1,31,40,','评论管理','40','/cms/comment/?status=2',NULL,'comment','1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('51','50','0,1,31,40,50,','查看','30',NULL,NULL,NULL,'0','cms:comment:view','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('52','50','0,1,31,40,50,','审核','40',NULL,NULL,NULL,'0','cms:comment:edit','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('53','40','0,1,31,40,','公共留言','80','/cms/guestbook/?status=2',NULL,'glass','1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('54','53','0,1,31,40,53,','查看','30',NULL,NULL,NULL,'0','cms:guestbook:view','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('55','53','0,1,31,40,53,','审核','40',NULL,NULL,NULL,'0','cms:guestbook:edit','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('56','71','0,1,27,71,','文件管理','90','/../static/ckfinder/ckfinder.html',NULL,'folder-open','1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('57','56','0,1,27,40,56,','查看','30',NULL,NULL,NULL,'0','cms:ckfinder:view','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('58','56','0,1,27,40,56,','上传','40',NULL,NULL,NULL,'0','cms:ckfinder:upload','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('59','56','0,1,27,40,56,','修改','50',NULL,NULL,NULL,'0','cms:ckfinder:edit','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('6','4','0,1,2,3,4,','修改','40',NULL,NULL,NULL,'0','sys:menu:edit','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('60','31','0,1,31,','统计分析','600',NULL,NULL,NULL,'1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('61','60','0,1,31,60,','信息量统计','30','/cms/stats/article',NULL,'tasks','1','cms:stats:article','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('62','1','0,1,','在线办公','200',NULL,NULL,NULL,'1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('63','62','0,1,62,','个人办公','30',NULL,NULL,NULL,'1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('64','63','0,1,62,63,','请假办理','300','/oa/leave',NULL,'leaf','0',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('65','64','0,1,62,63,64,','查看','30',NULL,NULL,NULL,'0','oa:leave:view','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('66','64','0,1,62,63,64,','修改','40',NULL,NULL,NULL,'0','oa:leave:edit','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('67','2','0,1,2,','日志查询','985',NULL,NULL,NULL,'1',NULL,'1','2013-06-03 08:00:00','1','2013-06-03 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('68','67','0,1,2,67,','日志查询','30','/sys/log',NULL,'pencil','1','sys:log:view','1','2013-06-03 08:00:00','1','2013-06-03 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('69','62','0,1,62,','流程管理','300',NULL,NULL,NULL,'1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('7','3','0,1,2,3,','角色管理','50','/sys/role/',NULL,'lock','1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('70','69','0,1,62,69,','流程管理','50','/act/process',NULL,'road','1','act:process:edit','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('71','27','0,1,27,','文件管理','90',NULL,NULL,NULL,'1',NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('72','69','0,1,62,69,','模型管理','100','/act/model',NULL,'road','1','act:model:edit','1','2013-09-20 08:00:00','1','2013-09-20 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('73','63','0,1,62,63,','我的任务','50','/act/task/todo/',NULL,'tasks','1',NULL,'1','2013-09-24 08:00:00','1','2013-09-24 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('74','63','0,1,62,63,','审批测试','100','/oa/testAudit',NULL,NULL,'1','oa:testAudit:view,oa:testAudit:edit','1','2013-09-24 08:00:00','1','2013-09-24 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('75','1','0,1,','在线演示','3000',NULL,NULL,NULL,'1',NULL,'1','2013-10-08 08:00:00','1','2013-10-08 08:00:00',NULL,'1'); INSERT INTO "public"."sys_menu" VALUES ('79','1','0,1,','代码生成','5000',NULL,NULL,NULL,'1',NULL,'1','2013-10-16 08:00:00','1','2013-10-16 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('8','7','0,1,2,3,7,','查看','30',NULL,NULL,NULL,'0','sys:role:view','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('80','79','0,1,79,','代码生成','50',NULL,NULL,NULL,'1',NULL,'1','2013-10-16 08:00:00','1','2013-10-16 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('81','80','0,1,79,80,','生成方案配置','30','/gen/genScheme',NULL,NULL,'1','gen:genScheme:view,gen:genScheme:edit','1','2013-10-16 08:00:00','1','2013-10-16 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('82','80','0,1,79,80,','业务表配置','20','/gen/genTable',NULL,NULL,'1','gen:genTable:view,gen:genTable:edit,gen:genTableColumn:view,gen:genTableColumn:edit','1','2013-10-16 08:00:00','1','2013-10-16 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('83','80','0,1,79,80,','代码模板管理','90','/gen/genTemplate',NULL,NULL,'1','gen:genTemplate:view,gen:genTemplate:edit','1','2013-10-16 08:00:00','1','2013-10-16 08:00:00',NULL,'1'); INSERT INTO "public"."sys_menu" VALUES ('84','67','0,1,2,67,','连接池监视','40','/../druid',NULL,NULL,'1',NULL,'1','2013-10-18 08:00:00','1','2013-10-18 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('85','76','0,1,75,76,','行政区域','80','/../static/map/map-city.html',NULL,NULL,'1',NULL,'1','2013-10-22 08:00:00','1','2013-10-22 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('86','75','0,1,75,','组件演示','50',NULL,NULL,NULL,'1',NULL,'1','2013-10-22 08:00:00','1','2013-10-22 08:00:00',NULL,'1'); INSERT INTO "public"."sys_menu" VALUES ('87','86','0,1,75,86,','组件演示','30','/test/test/form',NULL,NULL,'1','test:test:view,test:test:edit','1','2013-10-22 08:00:00','1','2013-10-22 08:00:00',NULL,'1'); INSERT INTO "public"."sys_menu" VALUES ('88','62','0,1,62,','通知通告','20','','','','1','','1','2013-11-08 08:00:00','1','2013-11-08 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('89','88','0,1,62,88,','我的通告','30','/oa/oaNotify/self','','','1','','1','2013-11-08 08:00:00','1','2013-11-08 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('9','7','0,1,2,3,7,','修改','40',NULL,NULL,NULL,'0','sys:role:edit','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('90','88','0,1,62,88,','通告管理','50','/oa/oaNotify','','','1','oa:oaNotify:view,oa:oaNotify:edit','1','2013-11-08 08:00:00','1','2013-11-08 08:00:00',NULL,'0'); INSERT INTO "public"."sys_menu" VALUES ('afab2db430e2457f9cf3a11feaa8b869','0ca004d6b1bf4bcab9670a5060d82a55','0,1,79,3c92c17886944d0687e73e286cada573,0ca004d6b1bf4bcab9670a5060d82a55,','编辑','60','','','','0','test:testTree:edit','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05','','0'); INSERT INTO "public"."sys_menu" VALUES ('b1f6d1b86ba24365bae7fd86c5082317','3c92c17886944d0687e73e286cada573','0,1,79,3c92c17886944d0687e73e286cada573,','主子表','60','/test/testDataMain','','','1','','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05','','0'); INSERT INTO "public"."sys_menu" VALUES ('ba8092291b40482db8fe7fc006ea3d76','3c92c17886944d0687e73e286cada573','0,1,79,3c92c17886944d0687e73e286cada573,','单表','30','/test/testData','','','1','','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05','','0'); INSERT INTO "public"."sys_menu" VALUES ('c2e4d9082a0b4386884a0b203afe2c5c','0ca004d6b1bf4bcab9670a5060d82a55','0,1,79,3c92c17886944d0687e73e286cada573,0ca004d6b1bf4bcab9670a5060d82a55,','查看','30','','','','0','test:testTree:view','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05','','0'); INSERT INTO "public"."sys_menu" VALUES ('d15ec45a4c5449c3bbd7a61d5f9dd1d2','b1f6d1b86ba24365bae7fd86c5082317','0,1,79,3c92c17886944d0687e73e286cada573,b1f6d1b86ba24365bae7fd86c5082317,','编辑','60','','','','0','test:testDataMain:edit','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05','','0'); INSERT INTO "public"."sys_menu" VALUES ('df7ce823c5b24ff9bada43d992f373e2','ba8092291b40482db8fe7fc006ea3d76','0,1,79,3c92c17886944d0687e73e286cada573,ba8092291b40482db8fe7fc006ea3d76,','查看','30','','','','0','test:testData:view','1','2013-08-12 13:10:05','1','2013-08-12 13:10:05','','0');
INSERT INTO "public"."sys_office" VALUES ('1','0','0,','山东省总公司','10','2','100000','1','1',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('10','7','0,1,7,','市场部','30','3','200003','2','2',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('11','7','0,1,7,','技术部','40','3','200004','2','2',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('12','7','0,1,7,','历城区分公司','0','4','201000','1','3',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('13','12','0,1,7,12,','公司领导','10','4','201001','2','3',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('14','12','0,1,7,12,','综合部','20','4','201002','2','3',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('15','12','0,1,7,12,','市场部','30','4','201003','2','3',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('16','12','0,1,7,12,','技术部','40','4','201004','2','3',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('17','7','0,1,7,','历下区分公司','40','5','201010','1','3',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('18','17','0,1,7,17,','公司领导','10','5','201011','2','3',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('19','17','0,1,7,17,','综合部','20','5','201012','2','3',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('2','1','0,1,','公司领导','10','2','100001','2','1',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('20','17','0,1,7,17,','市场部','30','5','201013','2','3',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('21','17','0,1,7,17,','技术部','40','5','201014','2','3',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('22','7','0,1,7,','高新区分公司','50','6','201010','1','3',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('23','22','0,1,7,22,','公司领导','10','6','201011','2','3',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('24','22','0,1,7,22,','综合部','20','6','201012','2','3',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('25','22','0,1,7,22,','市场部','30','6','201013','2','3',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('26','22','0,1,7,22,','技术部','40','6','201014','2','3',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('3','1','0,1,','综合部','20','2','100002','2','1',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('4','1','0,1,','市场部','30','2','100003','2','1',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('5','1','0,1,','技术部','40','2','100004','2','1',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('6','1','0,1,','研发部','50','2','100005','2','1',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('7','1','0,1,','济南市分公司','20','3','200000','1','2',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('8','7','0,1,7,','公司领导','10','3','200001','2','2',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_office" VALUES ('9','7','0,1,7,','综合部','20','3','200002','2','2',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0');
INSERT INTO "public"."sys_role" VALUES ('1','1','系统管理员','dept','assignment','1',NULL,'1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_role" VALUES ('2','1','公司管理员','hr','assignment','2',NULL,'1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_role" VALUES ('3','1','本公司管理员','a','assignment','3',NULL,'1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_role" VALUES ('4','1','部门管理员','b','assignment','4',NULL,'1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_role" VALUES ('5','1','本部门管理员','c','assignment','5',NULL,'1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_role" VALUES ('6','1','普通用户','d','assignment','8',NULL,'1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_role" VALUES ('7','7','济南市管理员','e','assignment','9',NULL,'1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0');
INSERT INTO "public"."sys_role_menu" VALUES ('1','1'); INSERT INTO "public"."sys_role_menu" VALUES ('1','10'); INSERT INTO "public"."sys_role_menu" VALUES ('1','11'); INSERT INTO "public"."sys_role_menu" VALUES ('1','12'); INSERT INTO "public"."sys_role_menu" VALUES ('1','13'); INSERT INTO "public"."sys_role_menu" VALUES ('1','14'); INSERT INTO "public"."sys_role_menu" VALUES ('1','15'); INSERT INTO "public"."sys_role_menu" VALUES ('1','16'); INSERT INTO "public"."sys_role_menu" VALUES ('1','17'); INSERT INTO "public"."sys_role_menu" VALUES ('1','18'); INSERT INTO "public"."sys_role_menu" VALUES ('1','19'); INSERT INTO "public"."sys_role_menu" VALUES ('1','2'); INSERT INTO "public"."sys_role_menu" VALUES ('1','20'); INSERT INTO "public"."sys_role_menu" VALUES ('1','21'); INSERT INTO "public"."sys_role_menu" VALUES ('1','22'); INSERT INTO "public"."sys_role_menu" VALUES ('1','23'); INSERT INTO "public"."sys_role_menu" VALUES ('1','24'); INSERT INTO "public"."sys_role_menu" VALUES ('1','25'); INSERT INTO "public"."sys_role_menu" VALUES ('1','26'); INSERT INTO "public"."sys_role_menu" VALUES ('1','27'); INSERT INTO "public"."sys_role_menu" VALUES ('1','28'); INSERT INTO "public"."sys_role_menu" VALUES ('1','29'); INSERT INTO "public"."sys_role_menu" VALUES ('1','3'); INSERT INTO "public"."sys_role_menu" VALUES ('1','30'); INSERT INTO "public"."sys_role_menu" VALUES ('1','31'); INSERT INTO "public"."sys_role_menu" VALUES ('1','32'); INSERT INTO "public"."sys_role_menu" VALUES ('1','33'); INSERT INTO "public"."sys_role_menu" VALUES ('1','34'); INSERT INTO "public"."sys_role_menu" VALUES ('1','35'); INSERT INTO "public"."sys_role_menu" VALUES ('1','36'); INSERT INTO "public"."sys_role_menu" VALUES ('1','37'); INSERT INTO "public"."sys_role_menu" VALUES ('1','38'); INSERT INTO "public"."sys_role_menu" VALUES ('1','39'); INSERT INTO "public"."sys_role_menu" VALUES ('1','4'); INSERT INTO "public"."sys_role_menu" VALUES ('1','40'); INSERT INTO "public"."sys_role_menu" VALUES ('1','41'); INSERT INTO "public"."sys_role_menu" VALUES ('1','42'); INSERT INTO "public"."sys_role_menu" VALUES ('1','43'); INSERT INTO "public"."sys_role_menu" VALUES ('1','44'); INSERT INTO "public"."sys_role_menu" VALUES ('1','45'); INSERT INTO "public"."sys_role_menu" VALUES ('1','46'); INSERT INTO "public"."sys_role_menu" VALUES ('1','47'); INSERT INTO "public"."sys_role_menu" VALUES ('1','48'); INSERT INTO "public"."sys_role_menu" VALUES ('1','49'); INSERT INTO "public"."sys_role_menu" VALUES ('1','5'); INSERT INTO "public"."sys_role_menu" VALUES ('1','50'); INSERT INTO "public"."sys_role_menu" VALUES ('1','51'); INSERT INTO "public"."sys_role_menu" VALUES ('1','52'); INSERT INTO "public"."sys_role_menu" VALUES ('1','53'); INSERT INTO "public"."sys_role_menu" VALUES ('1','54'); INSERT INTO "public"."sys_role_menu" VALUES ('1','55'); INSERT INTO "public"."sys_role_menu" VALUES ('1','56'); INSERT INTO "public"."sys_role_menu" VALUES ('1','57'); INSERT INTO "public"."sys_role_menu" VALUES ('1','58'); INSERT INTO "public"."sys_role_menu" VALUES ('1','59'); INSERT INTO "public"."sys_role_menu" VALUES ('1','6'); INSERT INTO "public"."sys_role_menu" VALUES ('1','60'); INSERT INTO "public"."sys_role_menu" VALUES ('1','61'); INSERT INTO "public"."sys_role_menu" VALUES ('1','62'); INSERT INTO "public"."sys_role_menu" VALUES ('1','63'); INSERT INTO "public"."sys_role_menu" VALUES ('1','64'); INSERT INTO "public"."sys_role_menu" VALUES ('1','65'); INSERT INTO "public"."sys_role_menu" VALUES ('1','66'); INSERT INTO "public"."sys_role_menu" VALUES ('1','67'); INSERT INTO "public"."sys_role_menu" VALUES ('1','68'); INSERT INTO "public"."sys_role_menu" VALUES ('1','69'); INSERT INTO "public"."sys_role_menu" VALUES ('1','7'); INSERT INTO "public"."sys_role_menu" VALUES ('1','70'); INSERT INTO "public"."sys_role_menu" VALUES ('1','71'); INSERT INTO "public"."sys_role_menu" VALUES ('1','72'); INSERT INTO "public"."sys_role_menu" VALUES ('1','73'); INSERT INTO "public"."sys_role_menu" VALUES ('1','74'); INSERT INTO "public"."sys_role_menu" VALUES ('1','75'); INSERT INTO "public"."sys_role_menu" VALUES ('1','76'); INSERT INTO "public"."sys_role_menu" VALUES ('1','77'); INSERT INTO "public"."sys_role_menu" VALUES ('1','78'); INSERT INTO "public"."sys_role_menu" VALUES ('1','79'); INSERT INTO "public"."sys_role_menu" VALUES ('1','8'); INSERT INTO "public"."sys_role_menu" VALUES ('1','80'); INSERT INTO "public"."sys_role_menu" VALUES ('1','81'); INSERT INTO "public"."sys_role_menu" VALUES ('1','82'); INSERT INTO "public"."sys_role_menu" VALUES ('1','83'); INSERT INTO "public"."sys_role_menu" VALUES ('1','84'); INSERT INTO "public"."sys_role_menu" VALUES ('1','85'); INSERT INTO "public"."sys_role_menu" VALUES ('1','86'); INSERT INTO "public"."sys_role_menu" VALUES ('1','87'); INSERT INTO "public"."sys_role_menu" VALUES ('1','88'); INSERT INTO "public"."sys_role_menu" VALUES ('1','89'); INSERT INTO "public"."sys_role_menu" VALUES ('1','9'); INSERT INTO "public"."sys_role_menu" VALUES ('1','90'); INSERT INTO "public"."sys_role_menu" VALUES ('2','1'); INSERT INTO "public"."sys_role_menu" VALUES ('2','10'); INSERT INTO "public"."sys_role_menu" VALUES ('2','11'); INSERT INTO "public"."sys_role_menu" VALUES ('2','12'); INSERT INTO "public"."sys_role_menu" VALUES ('2','13'); INSERT INTO "public"."sys_role_menu" VALUES ('2','14'); INSERT INTO "public"."sys_role_menu" VALUES ('2','15'); INSERT INTO "public"."sys_role_menu" VALUES ('2','16'); INSERT INTO "public"."sys_role_menu" VALUES ('2','17'); INSERT INTO "public"."sys_role_menu" VALUES ('2','18');
INSERT INTO "public"."sys_role_menu" VALUES ('2','19'); INSERT INTO "public"."sys_role_menu" VALUES ('2','2'); INSERT INTO "public"."sys_role_menu" VALUES ('2','20'); INSERT INTO "public"."sys_role_menu" VALUES ('2','21'); INSERT INTO "public"."sys_role_menu" VALUES ('2','22'); INSERT INTO "public"."sys_role_menu" VALUES ('2','23'); INSERT INTO "public"."sys_role_menu" VALUES ('2','24'); INSERT INTO "public"."sys_role_menu" VALUES ('2','25'); INSERT INTO "public"."sys_role_menu" VALUES ('2','26'); INSERT INTO "public"."sys_role_menu" VALUES ('2','27'); INSERT INTO "public"."sys_role_menu" VALUES ('2','28'); INSERT INTO "public"."sys_role_menu" VALUES ('2','29'); INSERT INTO "public"."sys_role_menu" VALUES ('2','3'); INSERT INTO "public"."sys_role_menu" VALUES ('2','30'); INSERT INTO "public"."sys_role_menu" VALUES ('2','31'); INSERT INTO "public"."sys_role_menu" VALUES ('2','32'); INSERT INTO "public"."sys_role_menu" VALUES ('2','33'); INSERT INTO "public"."sys_role_menu" VALUES ('2','34'); INSERT INTO "public"."sys_role_menu" VALUES ('2','35'); INSERT INTO "public"."sys_role_menu" VALUES ('2','36'); INSERT INTO "public"."sys_role_menu" VALUES ('2','37'); INSERT INTO "public"."sys_role_menu" VALUES ('2','38'); INSERT INTO "public"."sys_role_menu" VALUES ('2','39'); INSERT INTO "public"."sys_role_menu" VALUES ('2','4'); INSERT INTO "public"."sys_role_menu" VALUES ('2','40'); INSERT INTO "public"."sys_role_menu" VALUES ('2','41'); INSERT INTO "public"."sys_role_menu" VALUES ('2','42'); INSERT INTO "public"."sys_role_menu" VALUES ('2','43'); INSERT INTO "public"."sys_role_menu" VALUES ('2','44'); INSERT INTO "public"."sys_role_menu" VALUES ('2','45'); INSERT INTO "public"."sys_role_menu" VALUES ('2','46'); INSERT INTO "public"."sys_role_menu" VALUES ('2','47'); INSERT INTO "public"."sys_role_menu" VALUES ('2','48'); INSERT INTO "public"."sys_role_menu" VALUES ('2','49'); INSERT INTO "public"."sys_role_menu" VALUES ('2','5'); INSERT INTO "public"."sys_role_menu" VALUES ('2','50'); INSERT INTO "public"."sys_role_menu" VALUES ('2','51'); INSERT INTO "public"."sys_role_menu" VALUES ('2','52'); INSERT INTO "public"."sys_role_menu" VALUES ('2','53'); INSERT INTO "public"."sys_role_menu" VALUES ('2','54'); INSERT INTO "public"."sys_role_menu" VALUES ('2','55'); INSERT INTO "public"."sys_role_menu" VALUES ('2','56'); INSERT INTO "public"."sys_role_menu" VALUES ('2','57'); INSERT INTO "public"."sys_role_menu" VALUES ('2','58'); INSERT INTO "public"."sys_role_menu" VALUES ('2','59'); INSERT INTO "public"."sys_role_menu" VALUES ('2','6'); INSERT INTO "public"."sys_role_menu" VALUES ('2','60'); INSERT INTO "public"."sys_role_menu" VALUES ('2','61'); INSERT INTO "public"."sys_role_menu" VALUES ('2','62'); INSERT INTO "public"."sys_role_menu" VALUES ('2','63'); INSERT INTO "public"."sys_role_menu" VALUES ('2','64'); INSERT INTO "public"."sys_role_menu" VALUES ('2','65'); INSERT INTO "public"."sys_role_menu" VALUES ('2','66'); INSERT INTO "public"."sys_role_menu" VALUES ('2','67'); INSERT INTO "public"."sys_role_menu" VALUES ('2','68'); INSERT INTO "public"."sys_role_menu" VALUES ('2','69'); INSERT INTO "public"."sys_role_menu" VALUES ('2','7'); INSERT INTO "public"."sys_role_menu" VALUES ('2','70'); INSERT INTO "public"."sys_role_menu" VALUES ('2','71'); INSERT INTO "public"."sys_role_menu" VALUES ('2','72'); INSERT INTO "public"."sys_role_menu" VALUES ('2','73'); INSERT INTO "public"."sys_role_menu" VALUES ('2','74'); INSERT INTO "public"."sys_role_menu" VALUES ('2','75'); INSERT INTO "public"."sys_role_menu" VALUES ('2','76'); INSERT INTO "public"."sys_role_menu" VALUES ('2','77'); INSERT INTO "public"."sys_role_menu" VALUES ('2','78'); INSERT INTO "public"."sys_role_menu" VALUES ('2','79'); INSERT INTO "public"."sys_role_menu" VALUES ('2','8'); INSERT INTO "public"."sys_role_menu" VALUES ('2','80'); INSERT INTO "public"."sys_role_menu" VALUES ('2','81'); INSERT INTO "public"."sys_role_menu" VALUES ('2','82'); INSERT INTO "public"."sys_role_menu" VALUES ('2','83'); INSERT INTO "public"."sys_role_menu" VALUES ('2','84'); INSERT INTO "public"."sys_role_menu" VALUES ('2','85'); INSERT INTO "public"."sys_role_menu" VALUES ('2','86'); INSERT INTO "public"."sys_role_menu" VALUES ('2','87'); INSERT INTO "public"."sys_role_menu" VALUES ('2','88'); INSERT INTO "public"."sys_role_menu" VALUES ('2','89'); INSERT INTO "public"."sys_role_menu" VALUES ('2','9'); INSERT INTO "public"."sys_role_menu" VALUES ('2','90'); INSERT INTO "public"."sys_role_menu" VALUES ('3','1'); INSERT INTO "public"."sys_role_menu" VALUES ('3','10'); INSERT INTO "public"."sys_role_menu" VALUES ('3','11'); INSERT INTO "public"."sys_role_menu" VALUES ('3','12'); INSERT INTO "public"."sys_role_menu" VALUES ('3','13'); INSERT INTO "public"."sys_role_menu" VALUES ('3','14'); INSERT INTO "public"."sys_role_menu" VALUES ('3','15'); INSERT INTO "public"."sys_role_menu" VALUES ('3','16'); INSERT INTO "public"."sys_role_menu" VALUES ('3','17'); INSERT INTO "public"."sys_role_menu" VALUES ('3','18'); INSERT INTO "public"."sys_role_menu" VALUES ('3','19'); INSERT INTO "public"."sys_role_menu" VALUES ('3','2'); INSERT INTO "public"."sys_role_menu" VALUES ('3','20'); INSERT INTO "public"."sys_role_menu" VALUES ('3','21'); INSERT INTO "public"."sys_role_menu" VALUES ('3','22'); INSERT INTO "public"."sys_role_menu" VALUES ('3','23'); INSERT INTO "public"."sys_role_menu" VALUES ('3','24'); INSERT INTO "public"."sys_role_menu" VALUES ('3','25'); INSERT INTO "public"."sys_role_menu" VALUES ('3','26'); INSERT INTO "public"."sys_role_menu" VALUES ('3','27');
INSERT INTO "public"."sys_role_menu" VALUES ('3','28'); INSERT INTO "public"."sys_role_menu" VALUES ('3','29'); INSERT INTO "public"."sys_role_menu" VALUES ('3','3'); INSERT INTO "public"."sys_role_menu" VALUES ('3','30'); INSERT INTO "public"."sys_role_menu" VALUES ('3','31'); INSERT INTO "public"."sys_role_menu" VALUES ('3','32'); INSERT INTO "public"."sys_role_menu" VALUES ('3','33'); INSERT INTO "public"."sys_role_menu" VALUES ('3','34'); INSERT INTO "public"."sys_role_menu" VALUES ('3','35'); INSERT INTO "public"."sys_role_menu" VALUES ('3','36'); INSERT INTO "public"."sys_role_menu" VALUES ('3','37'); INSERT INTO "public"."sys_role_menu" VALUES ('3','38'); INSERT INTO "public"."sys_role_menu" VALUES ('3','39'); INSERT INTO "public"."sys_role_menu" VALUES ('3','4'); INSERT INTO "public"."sys_role_menu" VALUES ('3','40'); INSERT INTO "public"."sys_role_menu" VALUES ('3','41'); INSERT INTO "public"."sys_role_menu" VALUES ('3','42'); INSERT INTO "public"."sys_role_menu" VALUES ('3','43'); INSERT INTO "public"."sys_role_menu" VALUES ('3','44'); INSERT INTO "public"."sys_role_menu" VALUES ('3','45'); INSERT INTO "public"."sys_role_menu" VALUES ('3','46'); INSERT INTO "public"."sys_role_menu" VALUES ('3','47'); INSERT INTO "public"."sys_role_menu" VALUES ('3','48'); INSERT INTO "public"."sys_role_menu" VALUES ('3','49'); INSERT INTO "public"."sys_role_menu" VALUES ('3','5'); INSERT INTO "public"."sys_role_menu" VALUES ('3','50'); INSERT INTO "public"."sys_role_menu" VALUES ('3','51'); INSERT INTO "public"."sys_role_menu" VALUES ('3','52'); INSERT INTO "public"."sys_role_menu" VALUES ('3','53'); INSERT INTO "public"."sys_role_menu" VALUES ('3','54'); INSERT INTO "public"."sys_role_menu" VALUES ('3','55'); INSERT INTO "public"."sys_role_menu" VALUES ('3','56'); INSERT INTO "public"."sys_role_menu" VALUES ('3','57'); INSERT INTO "public"."sys_role_menu" VALUES ('3','58'); INSERT INTO "public"."sys_role_menu" VALUES ('3','59'); INSERT INTO "public"."sys_role_menu" VALUES ('3','6'); INSERT INTO "public"."sys_role_menu" VALUES ('3','60'); INSERT INTO "public"."sys_role_menu" VALUES ('3','61'); INSERT INTO "public"."sys_role_menu" VALUES ('3','62'); INSERT INTO "public"."sys_role_menu" VALUES ('3','63'); INSERT INTO "public"."sys_role_menu" VALUES ('3','64'); INSERT INTO "public"."sys_role_menu" VALUES ('3','65'); INSERT INTO "public"."sys_role_menu" VALUES ('3','66'); INSERT INTO "public"."sys_role_menu" VALUES ('3','67'); INSERT INTO "public"."sys_role_menu" VALUES ('3','68'); INSERT INTO "public"."sys_role_menu" VALUES ('3','69'); INSERT INTO "public"."sys_role_menu" VALUES ('3','7'); INSERT INTO "public"."sys_role_menu" VALUES ('3','70'); INSERT INTO "public"."sys_role_menu" VALUES ('3','71'); INSERT INTO "public"."sys_role_menu" VALUES ('3','72'); INSERT INTO "public"."sys_role_menu" VALUES ('3','73'); INSERT INTO "public"."sys_role_menu" VALUES ('3','74'); INSERT INTO "public"."sys_role_menu" VALUES ('3','75'); INSERT INTO "public"."sys_role_menu" VALUES ('3','76'); INSERT INTO "public"."sys_role_menu" VALUES ('3','77'); INSERT INTO "public"."sys_role_menu" VALUES ('3','78'); INSERT INTO "public"."sys_role_menu" VALUES ('3','79'); INSERT INTO "public"."sys_role_menu" VALUES ('3','8'); INSERT INTO "public"."sys_role_menu" VALUES ('3','80'); INSERT INTO "public"."sys_role_menu" VALUES ('3','81'); INSERT INTO "public"."sys_role_menu" VALUES ('3','82'); INSERT INTO "public"."sys_role_menu" VALUES ('3','83'); INSERT INTO "public"."sys_role_menu" VALUES ('3','84'); INSERT INTO "public"."sys_role_menu" VALUES ('3','85'); INSERT INTO "public"."sys_role_menu" VALUES ('3','86'); INSERT INTO "public"."sys_role_menu" VALUES ('3','87'); INSERT INTO "public"."sys_role_menu" VALUES ('3','88'); INSERT INTO "public"."sys_role_menu" VALUES ('3','89'); INSERT INTO "public"."sys_role_menu" VALUES ('3','9'); INSERT INTO "public"."sys_role_menu" VALUES ('3','90');
INSERT INTO "public"."sys_role_office" VALUES ('7','10'); INSERT INTO "public"."sys_role_office" VALUES ('7','11'); INSERT INTO "public"."sys_role_office" VALUES ('7','12'); INSERT INTO "public"."sys_role_office" VALUES ('7','13'); INSERT INTO "public"."sys_role_office" VALUES ('7','14'); INSERT INTO "public"."sys_role_office" VALUES ('7','15'); INSERT INTO "public"."sys_role_office" VALUES ('7','16'); INSERT INTO "public"."sys_role_office" VALUES ('7','17'); INSERT INTO "public"."sys_role_office" VALUES ('7','18'); INSERT INTO "public"."sys_role_office" VALUES ('7','19'); INSERT INTO "public"."sys_role_office" VALUES ('7','20'); INSERT INTO "public"."sys_role_office" VALUES ('7','21'); INSERT INTO "public"."sys_role_office" VALUES ('7','22'); INSERT INTO "public"."sys_role_office" VALUES ('7','23'); INSERT INTO "public"."sys_role_office" VALUES ('7','24'); INSERT INTO "public"."sys_role_office" VALUES ('7','25'); INSERT INTO "public"."sys_role_office" VALUES ('7','26'); INSERT INTO "public"."sys_role_office" VALUES ('7','7'); INSERT INTO "public"."sys_role_office" VALUES ('7','8'); INSERT INTO "public"."sys_role_office" VALUES ('7','9');
INSERT INTO "public"."sys_user" VALUES ('1','1','2','thinkgem','02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032','0001','系统管理员','thinkgem@163.com','8675','8675',NULL,NULL,NULL,NULL,'1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00','最高管理员','0'); INSERT INTO "public"."sys_user" VALUES ('10','7','11','jn_jsb','02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032','0010','济南技术部',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_user" VALUES ('11','12','13','lc_admin','02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032','0011','济南历城领导',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_user" VALUES ('12','12','18','lx_admin','02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032','0012','济南历下领导',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_user" VALUES ('13','22','23','gx_admin','02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032','0013','济南高新领导',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_user" VALUES ('2','1','2','sd_admin','02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032','0002','管理员',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_user" VALUES ('3','1','3','sd_zhb','02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032','0003','综合部',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_user" VALUES ('4','1','4','sd_scb','02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032','0004','市场部',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_user" VALUES ('5','1','5','sd_jsb','02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032','0005','技术部',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_user" VALUES ('6','1','6','sd_yfb','02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032','0006','研发部',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_user" VALUES ('7','7','8','jn_admin','02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032','0007','济南领导',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_user" VALUES ('8','7','9','jn_zhb','02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032','0008','济南综合部',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0'); INSERT INTO "public"."sys_user" VALUES ('9','7','10','jn_scb','02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032','0009','济南市场部',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','1','2013-05-27 08:00:00','1','2013-05-27 08:00:00',NULL,'0');
INSERT INTO "public"."sys_user_role" VALUES ('1','1'); INSERT INTO "public"."sys_user_role" VALUES ('1','2'); INSERT INTO "public"."sys_user_role" VALUES ('10','2'); INSERT INTO "public"."sys_user_role" VALUES ('11','3'); INSERT INTO "public"."sys_user_role" VALUES ('12','4'); INSERT INTO "public"."sys_user_role" VALUES ('13','5'); INSERT INTO "public"."sys_user_role" VALUES ('14','6'); INSERT INTO "public"."sys_user_role" VALUES ('2','1'); INSERT INTO "public"."sys_user_role" VALUES ('3','2'); INSERT INTO "public"."sys_user_role" VALUES ('4','3'); INSERT INTO "public"."sys_user_role" VALUES ('5','4'); INSERT INTO "public"."sys_user_role" VALUES ('6','5'); INSERT INTO "public"."sys_user_role" VALUES ('7','2'); INSERT INTO "public"."sys_user_role" VALUES ('7','7'); INSERT INTO "public"."sys_user_role" VALUES ('8','2'); INSERT INTO "public"."sys_user_role" VALUES ('9','1');

-- ----------------------------
--  Index definition for "public"."act_ge_bytearray"
-- ----------------------------
CREATE INDEX "act_idx_bytear_depl" ON "public"."act_ge_bytearray" USING btree (deployment_id_);


-- ----------------------------
--  Index definition for "public"."act_hi_actinst"
-- ----------------------------
CREATE INDEX "act_idx_hi_act_inst_end" ON "public"."act_hi_actinst" USING btree (end_time_);

CREATE INDEX "act_idx_hi_act_inst_exec" ON "public"."act_hi_actinst" USING btree (execution_id_, act_id_);

CREATE INDEX "act_idx_hi_act_inst_procinst" ON "public"."act_hi_actinst" USING btree (proc_inst_id_, act_id_);

CREATE INDEX "act_idx_hi_act_inst_start" ON "public"."act_hi_actinst" USING btree (start_time_);


-- ----------------------------
--  Index definition for "public"."act_hi_detail"
-- ----------------------------
CREATE INDEX "act_idx_hi_detail_act_inst" ON "public"."act_hi_detail" USING btree (act_inst_id_);

CREATE INDEX "act_idx_hi_detail_name" ON "public"."act_hi_detail" USING btree (name_);

CREATE INDEX "act_idx_hi_detail_proc_inst" ON "public"."act_hi_detail" USING btree (proc_inst_id_);

CREATE INDEX "act_idx_hi_detail_task_id" ON "public"."act_hi_detail" USING btree (task_id_);

CREATE INDEX "act_idx_hi_detail_time" ON "public"."act_hi_detail" USING btree (time_);


-- ----------------------------
--  Index definition for "public"."act_hi_identitylink"
-- ----------------------------
CREATE INDEX "act_idx_hi_ident_lnk_procinst" ON "public"."act_hi_identitylink" USING btree (proc_inst_id_);

CREATE INDEX "act_idx_hi_ident_lnk_task" ON "public"."act_hi_identitylink" USING btree (task_id_);

CREATE INDEX "act_idx_hi_ident_lnk_user" ON "public"."act_hi_identitylink" USING btree (user_id_);


-- ----------------------------
--  Index definition for "public"."act_hi_procinst"
-- ----------------------------
CREATE INDEX "act_idx_hi_pro_i_buskey" ON "public"."act_hi_procinst" USING btree (business_key_);

CREATE INDEX "act_idx_hi_pro_inst_end" ON "public"."act_hi_procinst" USING btree (end_time_);


-- ----------------------------
--  Index definition for "public"."act_hi_taskinst"
-- ----------------------------
CREATE INDEX "act_idx_hi_task_inst_procinst" ON "public"."act_hi_taskinst" USING btree (proc_inst_id_);


-- ----------------------------
--  Index definition for "public"."act_hi_varinst"
-- ----------------------------
CREATE INDEX "act_idx_hi_procvar_name_type" ON "public"."act_hi_varinst" USING btree (name_, var_type_);

CREATE INDEX "act_idx_hi_procvar_proc_inst" ON "public"."act_hi_varinst" USING btree (proc_inst_id_);

CREATE INDEX "act_idx_hi_procvar_task_id" ON "public"."act_hi_varinst" USING btree (task_id_);


-- ----------------------------
--  Index definition for "public"."act_id_membership"
-- ----------------------------
CREATE INDEX "act_idx_memb_group" ON "public"."act_id_membership" USING btree (group_id_);

CREATE INDEX "act_idx_memb_user" ON "public"."act_id_membership" USING btree (user_id_);


-- ----------------------------
--  Index definition for "public"."act_procdef_info"
-- ----------------------------
CREATE INDEX "act_idx_procdef_info_json" ON "public"."act_procdef_info" USING btree (info_json_id_);

CREATE INDEX "act_idx_procdef_info_proc" ON "public"."act_procdef_info" USING btree (proc_def_id_);


-- ----------------------------
--  Index definition for "public"."act_re_model"
-- ----------------------------
CREATE INDEX "act_idx_model_deployment" ON "public"."act_re_model" USING btree (deployment_id_);

CREATE INDEX "act_idx_model_source" ON "public"."act_re_model" USING btree (editor_source_value_id_);

CREATE INDEX "act_idx_model_source_extra" ON "public"."act_re_model" USING btree (editor_source_extra_value_id_);


-- ----------------------------
--  Index definition for "public"."act_ru_event_subscr"
-- ----------------------------
CREATE INDEX "act_idx_event_subscr" ON "public"."act_ru_event_subscr" USING btree (execution_id_);

CREATE INDEX "act_idx_event_subscr_config_" ON "public"."act_ru_event_subscr" USING btree (configuration_);


-- ----------------------------
--  Index definition for "public"."act_ru_execution"
-- ----------------------------
CREATE INDEX "act_idx_exe_parent" ON "public"."act_ru_execution" USING btree (parent_id_);

CREATE INDEX "act_idx_exe_procdef" ON "public"."act_ru_execution" USING btree (proc_def_id_);

CREATE INDEX "act_idx_exe_procinst" ON "public"."act_ru_execution" USING btree (proc_inst_id_);

CREATE INDEX "act_idx_exe_super" ON "public"."act_ru_execution" USING btree (super_exec_);

CREATE INDEX "act_idx_exec_buskey" ON "public"."act_ru_execution" USING btree (business_key_);


-- ----------------------------
--  Index definition for "public"."act_ru_identitylink"
-- ----------------------------
CREATE INDEX "act_idx_athrz_procedef" ON "public"."act_ru_identitylink" USING btree (proc_def_id_);

CREATE INDEX "act_idx_ident_lnk_group" ON "public"."act_ru_identitylink" USING btree (group_id_);

CREATE INDEX "act_idx_ident_lnk_user" ON "public"."act_ru_identitylink" USING btree (user_id_);

CREATE INDEX "act_idx_idl_procinst" ON "public"."act_ru_identitylink" USING btree (proc_inst_id_);

CREATE INDEX "act_idx_tskass_task" ON "public"."act_ru_identitylink" USING btree (task_id_);


-- ----------------------------
--  Index definition for "public"."act_ru_job"
-- ----------------------------
CREATE INDEX "act_idx_job_exception" ON "public"."act_ru_job" USING btree (exception_stack_id_);


-- ----------------------------
--  Index definition for "public"."act_ru_task"
-- ----------------------------
CREATE INDEX "act_idx_task_create" ON "public"."act_ru_task" USING btree (create_time_);

CREATE INDEX "act_idx_task_exec" ON "public"."act_ru_task" USING btree (execution_id_);

CREATE INDEX "act_idx_task_procdef" ON "public"."act_ru_task" USING btree (proc_def_id_);

CREATE INDEX "act_idx_task_procinst" ON "public"."act_ru_task" USING btree (proc_inst_id_);


-- ----------------------------
--  Index definition for "public"."act_ru_variable"
-- ----------------------------
CREATE INDEX "act_idx_var_bytearray" ON "public"."act_ru_variable" USING btree (bytearray_id_);

CREATE INDEX "act_idx_var_exe" ON "public"."act_ru_variable" USING btree (execution_id_);

CREATE INDEX "act_idx_var_procinst" ON "public"."act_ru_variable" USING btree (proc_inst_id_);

CREATE INDEX "act_idx_variable_task_id" ON "public"."act_ru_variable" USING btree (task_id_);


-- ----------------------------
--  Index definition for "public"."cms_article"
-- ----------------------------
CREATE INDEX "cms_article_category_id" ON "public"."cms_article" USING btree (category_id);

CREATE INDEX "cms_article_create_by" ON "public"."cms_article" USING btree (create_by);

CREATE INDEX "cms_article_del_flag" ON "public"."cms_article" USING btree (del_flag);

CREATE INDEX "cms_article_keywords" ON "public"."cms_article" USING btree (keywords);

CREATE INDEX "cms_article_title" ON "public"."cms_article" USING btree (title);

CREATE INDEX "cms_article_update_date" ON "public"."cms_article" USING btree (update_date);

CREATE INDEX "cms_article_weight" ON "public"."cms_article" USING btree (weight);


-- ----------------------------
--  Index definition for "public"."cms_category"
-- ----------------------------
CREATE INDEX "cms_category_del_flag" ON "public"."cms_category" USING btree (del_flag);

CREATE INDEX "cms_category_module" ON "public"."cms_category" USING btree (module);

CREATE INDEX "cms_category_name" ON "public"."cms_category" USING btree (name);

CREATE INDEX "cms_category_office_id" ON "public"."cms_category" USING btree (office_id);

CREATE INDEX "cms_category_parent_id" ON "public"."cms_category" USING btree (parent_id);

CREATE INDEX "cms_category_site_id" ON "public"."cms_category" USING btree (site_id);

CREATE INDEX "cms_category_sort" ON "public"."cms_category" USING btree (sort);


-- ----------------------------
--  Index definition for "public"."cms_comment"
-- ----------------------------
CREATE INDEX "cms_comment_category_id" ON "public"."cms_comment" USING btree (category_id);

CREATE INDEX "cms_comment_content_id" ON "public"."cms_comment" USING btree (content_id);

CREATE INDEX "cms_comment_status" ON "public"."cms_comment" USING btree (del_flag);


-- ----------------------------
--  Index definition for "public"."cms_guestbook"
-- ----------------------------
CREATE INDEX "cms_guestbook_del_flag" ON "public"."cms_guestbook" USING btree (del_flag);


-- ----------------------------
--  Index definition for "public"."cms_link"
-- ----------------------------
CREATE INDEX "cms_link_category_id" ON "public"."cms_link" USING btree (category_id);

CREATE INDEX "cms_link_create_by" ON "public"."cms_link" USING btree (create_by);

CREATE INDEX "cms_link_del_flag" ON "public"."cms_link" USING btree (del_flag);

CREATE INDEX "cms_link_title" ON "public"."cms_link" USING btree (title);

CREATE INDEX "cms_link_update_date" ON "public"."cms_link" USING btree (update_date);

CREATE INDEX "cms_link_weight" ON "public"."cms_link" USING btree (weight);


-- ----------------------------
--  Index definition for "public"."gen_scheme"
-- ----------------------------
CREATE INDEX "gen_scheme_del_flag" ON "public"."gen_scheme" USING btree (del_flag);


-- ----------------------------
--  Index definition for "public"."gen_table"
-- ----------------------------
CREATE INDEX "gen_table_del_flag" ON "public"."gen_table" USING btree (del_flag);

CREATE INDEX "gen_table_name" ON "public"."gen_table" USING btree (name);


-- ----------------------------
--  Index definition for "public"."gen_table_column"
-- ----------------------------
CREATE INDEX "gen_table_column_del_flag" ON "public"."gen_table_column" USING btree (del_flag);

CREATE INDEX "gen_table_column_name" ON "public"."gen_table_column" USING btree (name);

CREATE INDEX "gen_table_column_sort" ON "public"."gen_table_column" USING btree (sort);

CREATE INDEX "gen_table_column_table_id" ON "public"."gen_table_column" USING btree (gen_table_id);


-- ----------------------------
--  Index definition for "public"."gen_template"
-- ----------------------------
CREATE INDEX "gen_template_del_falg" ON "public"."gen_template" USING btree (del_flag);


-- ----------------------------
--  Index definition for "public"."oa_leave"
-- ----------------------------
CREATE INDEX "oa_leave_create_by" ON "public"."oa_leave" USING btree (create_by);

CREATE INDEX "oa_leave_del_flag" ON "public"."oa_leave" USING btree (del_flag);

CREATE INDEX "oa_leave_process_instance_id" ON "public"."oa_leave" USING btree (process_instance_id);


-- ----------------------------
--  Index definition for "public"."oa_notify"
-- ----------------------------
CREATE INDEX "oa_notify_del_flag" ON "public"."oa_notify" USING btree (del_flag);


-- ----------------------------
--  Index definition for "public"."oa_notify_record"
-- ----------------------------
CREATE INDEX "oa_notify_record_notify_id" ON "public"."oa_notify_record" USING btree (oa_notify_id);

CREATE INDEX "oa_notify_record_read_flag" ON "public"."oa_notify_record" USING btree (read_flag);

CREATE INDEX "oa_notify_record_user_id" ON "public"."oa_notify_record" USING btree (user_id);


-- ----------------------------
--  Index definition for "public"."oa_test_audit"
-- ----------------------------
CREATE INDEX "OA_TEST_AUDIT_del_flag" ON "public"."oa_test_audit" USING btree (del_flag);


-- ----------------------------
--  Index definition for "public"."sys_area"
-- ----------------------------
CREATE INDEX "sys_area_del_flag" ON "public"."sys_area" USING btree (del_flag);

CREATE INDEX "sys_area_parent_id" ON "public"."sys_area" USING btree (parent_id);


-- ----------------------------
--  Index definition for "public"."sys_dict"
-- ----------------------------
CREATE INDEX "sys_dict_del_flag" ON "public"."sys_dict" USING btree (del_flag);

CREATE INDEX "sys_dict_label" ON "public"."sys_dict" USING btree (label);

CREATE INDEX "sys_dict_value" ON "public"."sys_dict" USING btree (value);


-- ----------------------------
--  Index definition for "public"."sys_log"
-- ----------------------------
CREATE INDEX "sys_log_create_by" ON "public"."sys_log" USING btree (create_by);

CREATE INDEX "sys_log_create_date" ON "public"."sys_log" USING btree (create_date);

CREATE INDEX "sys_log_request_uri" ON "public"."sys_log" USING btree (request_uri);

CREATE INDEX "sys_log_type" ON "public"."sys_log" USING btree (type);


-- ----------------------------
--  Index definition for "public"."sys_mdict"
-- ----------------------------
CREATE INDEX "sys_mdict_del_flag" ON "public"."sys_mdict" USING btree (del_flag);

CREATE INDEX "sys_mdict_parent_id" ON "public"."sys_mdict" USING btree (parent_id);


-- ----------------------------
--  Index definition for "public"."sys_menu"
-- ----------------------------
CREATE INDEX "sys_menu_del_flag" ON "public"."sys_menu" USING btree (del_flag);

CREATE INDEX "sys_menu_parent_id" ON "public"."sys_menu" USING btree (parent_id);


-- ----------------------------
--  Index definition for "public"."sys_office"
-- ----------------------------
CREATE INDEX "sys_office_del_flag" ON "public"."sys_office" USING btree (del_flag);

CREATE INDEX "sys_office_parent_id" ON "public"."sys_office" USING btree (parent_id);

CREATE INDEX "sys_office_type" ON "public"."sys_office" USING btree (type);


-- ----------------------------
--  Index definition for "public"."sys_role"
-- ----------------------------
CREATE INDEX "sys_role_del_flag" ON "public"."sys_role" USING btree (del_flag);

CREATE INDEX "sys_role_enname" ON "public"."sys_role" USING btree (enname);


-- ----------------------------
--  Index definition for "public"."sys_user"
-- ----------------------------
CREATE INDEX "sys_user_company_id" ON "public"."sys_user" USING btree (company_id);

CREATE INDEX "sys_user_del_flag" ON "public"."sys_user" USING btree (del_flag);

CREATE INDEX "sys_user_login_name" ON "public"."sys_user" USING btree (login_name);

CREATE INDEX "sys_user_office_id" ON "public"."sys_user" USING btree (office_id);

CREATE INDEX "sys_user_update_date" ON "public"."sys_user" USING btree (update_date);


-- ----------------------------
--  Index definition for "public"."test_data"
-- ----------------------------
CREATE INDEX "test_data_del_flag" ON "public"."test_data" USING btree (del_flag);


-- ----------------------------
--  Index definition for "public"."test_data_child"
-- ----------------------------
CREATE INDEX "test_data_child_del_flag" ON "public"."test_data_child" USING btree (del_flag);


-- ----------------------------
--  Index definition for "public"."test_data_main"
-- ----------------------------
CREATE INDEX "test_data_main_del_flag" ON "public"."test_data_main" USING btree (del_flag);


-- ----------------------------
--  Index definition for "public"."test_tree"
-- ----------------------------
CREATE INDEX "test_data_parent_id" ON "public"."test_tree" USING btree (parent_id);

CREATE INDEX "test_tree_del_flag" ON "public"."test_tree" USING btree (del_flag);

