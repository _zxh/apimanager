/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.apimanager.service;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.apimanager.dao.ConsumerDao;
import com.thinkgem.jeesite.modules.apimanager.entity.Consumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 接口用户Service
 * @author zxh
 * @version 2016-09-12
 */
@Service
@Transactional(readOnly = true)
public class ConsumerService extends CrudService<ConsumerDao, Consumer> {
	@Autowired
	private ConsumerDao consumerDao;
	public Consumer get(String id) {
		return super.get(id);
	}
	
	public List<Consumer> findList(Consumer consumer) {
		return super.findList(consumer);
	}
	
	public Page<Consumer> findPage(Page<Consumer> page, Consumer consumer) {
		return super.findPage(page, consumer);
	}
	
	@Transactional(readOnly = false)
	public void save(Consumer consumer) {
		super.save(consumer);
	}
	
	@Transactional(readOnly = false)
	public void delete(Consumer consumer) {
		super.delete(consumer);
	}
	
}