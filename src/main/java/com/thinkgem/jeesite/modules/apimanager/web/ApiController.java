/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.apimanager.web;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.BeanUtils;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.web.Result;
import com.thinkgem.jeesite.modules.apimanager.entity.Api;
import com.thinkgem.jeesite.modules.apimanager.service.ApiService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 接口Controller
 * @author zxh
 * @version 2016-09-12
 */
@Controller
@RequestMapping(value = "${adminPath}/apimanager/api")
public class ApiController extends BaseController {

	@Autowired
	private ApiService apiService;
	
	@ModelAttribute
	public Api getModel(@RequestParam(required=false) String id) {
		Api entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = apiService.get(id);
		}
		if (entity == null){
			entity = new Api();
		}
		return entity;
	}
	
	@RequiresPermissions("apimanager:api:view")
	@RequestMapping(value = {"list", ""})
	public String list(Api api, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Api> page = apiService.findPage(new Page<Api>(request, response), api); 
		model.addAttribute("page", page);
		return "modules/apimanager/apiList";
	}

	@RequiresPermissions("apimanager:api:view")
	@RequestMapping(value = "form")
	public String form(Api api, Model model) {
		model.addAttribute("api", api);
		return "modules/apimanager/apiForm";
	}

	@RequiresPermissions("apimanager:api:edit")
	@RequestMapping(value = "save")
	public String save(Api api, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, api)){
			return form(api, model);
		}
		apiService.save(api);
		addMessage(redirectAttributes, "保存接口成功");
		return "redirect:"+Global.getAdminPath()+"/apimanager/api/list?repage";
	}
	
	@RequiresPermissions("apimanager:api:edit")
	@RequestMapping(value = "delete")
	public String delete(Api api, RedirectAttributes redirectAttributes) {
		apiService.delete(api);
		addMessage(redirectAttributes, "删除接口成功");
		return "redirect:"+Global.getAdminPath()+"/apimanager/api/list?repage";
	}

	/**
	 * 更新接口
	 * @param id
	 * @param api
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="{id}",method = RequestMethod.PATCH)
	public Result patch(@PathVariable String id, @RequestBody Api api){
		Api oldApi=apiService.get(id);
		BeanUtils.copyPropertiesIgnoreNull(api,oldApi);
		oldApi.setIsNewRecord(false);//copy后不知道为啥isNewRecord变成true了
		apiService.save(oldApi);
		return new Result(true, 200, "", oldApi);
	}

	@ResponseBody
	@RequestMapping(value="{id}",method = RequestMethod.GET)
	public Result get(@PathVariable String id){
		Api api=apiService.get(id);
		return new Result(true, 200, "", api);
	}
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET)
	public Result get(Api api, HttpServletRequest request, HttpServletResponse response){
		List<Api> list = apiService.findList(api);
		return new Result(true, 200, "", list);
	}

}