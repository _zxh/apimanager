/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.apimanager.service;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.apimanager.dao.ApiDao;
import com.thinkgem.jeesite.modules.apimanager.dao.ConsumerDao;
import com.thinkgem.jeesite.modules.apimanager.dao.ProviderDao;
import com.thinkgem.jeesite.modules.apimanager.entity.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 服务Service
 * @author zxh
 * @version 2016-09-12
 */
@Service
@Transactional(readOnly = true)
public class ProviderService extends CrudService<ProviderDao, Provider> {
	@Autowired
	private ConsumerDao consumerDao;
	@Autowired
	private ApiDao apiDao;
	public Provider get(String id) {
		return super.get(id);
	}
	
	public List<Provider> findList(Provider provider) {
		return super.findList(provider);
	}
	
	public Page<Provider> findPage(Page<Provider> page, Provider provider) {
		return super.findPage(page, provider);
	}
	
	@Transactional(readOnly = false)
	public void save(Provider provider) {
		super.save(provider);
	}
	
	@Transactional(readOnly = false)
	public void delete(Provider provider) {
		super.delete(provider);
	}

	public List<Provider> findListByName(String name) {
		return super.dao.findListByName(name);
	}
}
