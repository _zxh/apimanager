/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.apimanager.service;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.apimanager.dao.ApiDao;
import com.thinkgem.jeesite.modules.apimanager.dao.ConsumerDao;
import com.thinkgem.jeesite.modules.apimanager.entity.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 接口Service
 * @author zxh
 * @version 2016-09-12
 */
@Service
@Transactional(readOnly = true)
public class ApiService extends CrudService<ApiDao, Api> {
	@Autowired
	private ConsumerDao consumerDao;

	public Api get(String id) {
		return super.get(id);
	}
	
	public List<Api> findList(Api api) {
		return super.findList(api);
	}
	
	public Page<Api> findPage(Page<Api> page, Api api) {
		return super.findPage(page, api);
	}
	
	@Transactional(readOnly = false)
	public void save(Api api) {
		super.save(api);
	}
	
	@Transactional(readOnly = false)
	public void delete(Api api) {
		super.delete(api);
	}
	
}