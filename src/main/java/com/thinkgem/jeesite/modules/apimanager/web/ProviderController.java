/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.apimanager.web;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.web.Result;
import com.thinkgem.jeesite.modules.apimanager.entity.Api;
import com.thinkgem.jeesite.modules.apimanager.entity.Provider;
import com.thinkgem.jeesite.modules.apimanager.service.ApiService;
import com.thinkgem.jeesite.modules.apimanager.service.ProviderService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 服务Controller
 * @author zxh
 * @version 2016-09-12
 */
@Controller
@RequestMapping(value = "${adminPath}/apimanager/provider")
public class ProviderController extends BaseController {

	@Autowired
	private ProviderService providerService;
	@Autowired
	private ApiService apiService;
	
	@ModelAttribute
	public Provider getModel(@RequestParam(required=false) String id) {
		Provider entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = providerService.get(id);
		}
		if (entity == null){
			entity = new Provider();
		}
		return entity;
	}
	
	@RequiresPermissions("apimanager:provider:view")
	@RequestMapping(value = {"list", ""})
	public String list(Provider provider, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Provider> page = providerService.findPage(new Page<Provider>(request, response), provider); 
		model.addAttribute("page", page);
		return "modules/apimanager/providerList";
	}

	@RequiresPermissions("apimanager:provider:view")
	@RequestMapping(value = "form")
	public String form(Provider provider, Model model) {
		model.addAttribute("provider", provider);
		return "modules/apimanager/providerForm";
	}

	@RequiresPermissions("apimanager:provider:edit")
	@RequestMapping(value = "save")
	public String save(Provider provider, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, provider)){
			return form(provider, model);
		}
		List<Provider> list = providerService.findListByName(provider.getName());
		if(StringUtils.isEmpty(provider.getId())&&list!=null&&list.size()>0){//
			model.addAttribute("message", "已存在服务名为"+provider.getName()+"的服务,添加失败");
			return form(provider, model);
		}else if(list!=null&&list.size()>0&&!list.get(0).getId().equals(provider.getId())){
			model.addAttribute("message", "已存在服务名为"+provider.getName()+"的服务,更新失败");
			return form(provider, model);
		}
		providerService.save(provider);
		addMessage(redirectAttributes, "保存服务成功");
		return "redirect:"+Global.getAdminPath()+"/apimanager/provider/list?repage";
	}
	
	@RequiresPermissions("apimanager:provider:edit")
	@RequestMapping(value = "delete")
	public String delete(Provider provider, RedirectAttributes redirectAttributes) {
		providerService.delete(provider);
		addMessage(redirectAttributes, "删除服务成功");
		return "redirect:"+Global.getAdminPath()+"/apimanager/provider/list?repage";
	}

	/**
	 * 获取列表
	 * @return
     */
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET)
	public Result get(Provider provider, HttpServletRequest request, HttpServletResponse response){
		List<Provider> list = providerService.findList(provider);
		return new Result(true, 200, "", list);
	}
	/**
	 * 获取子列表
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="{provider_id}/apis",method = RequestMethod.GET)
	public Result get(@PathVariable("provider_id") String provider_id, Api api, HttpServletRequest request, HttpServletResponse response){
		api.setProviderId(provider_id);
		List<Api> list = apiService.findList(api);
		return new Result(true, 200, "", list);
	}

	/**
	 * 创建
	 * @param provider
	 * @return
     */
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public Result post(@RequestBody Provider provider){
		Result r=beanValidatorResult(provider);
		if(!r.isSucc()){
			return r;
		}
		List<Provider> list = providerService.findListByName(provider.getName());
		if(list!=null&&list.size()>0){
			return new Result(false, 200, "已存在服务名为"+provider.getName()+"的服务", provider);
		}else{
			providerService.save(provider);
		}
		return new Result(true, 200, "", provider);
	}

	/**
	 * 获取单个对象
	 * @param id
	 * @return
     */
	@ResponseBody
	@RequestMapping(value="{id}",method = RequestMethod.GET)
	public Result get(@PathVariable String id){
		Provider provider=providerService.get(id);
		return new Result(true, 200, "", provider);
	}

	/**
	 * 删除
	 * @param id
	 * @return
     */
	@ResponseBody
	@RequestMapping(value="{id}",method = RequestMethod.DELETE)
	public Result delete(@PathVariable String id){
		providerService.delete(providerService.get(id));
		return new Result();
	}

	/**
	 * 更新
	 * @param id
	 * @param provider
     * @return
     */
	@ResponseBody
	@RequestMapping(value="{id}",method = RequestMethod.PUT)
	public Result put(@PathVariable String id,@RequestBody Provider provider){
		Result r=beanValidatorResult(provider);
		if(!r.isSucc()){
			return r;
		}
		List<Provider> list = providerService.findListByName(provider.getName());
		if(list!=null&&list.size()>0&&!list.get(0).getId().equals(provider.getId())){
			return new Result(false, 200, "已存在服务名为"+provider.getName()+"的服务", provider);
		}else{
			provider.setId(id);
			providerService.save(provider);
			return new Result(true, 200, "", provider);
		}


	}


}