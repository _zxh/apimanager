/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.apimanager.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.apimanager.entity.Provider;

import java.util.List;

/**
 * 服务DAO接口
 * @author zxh
 * @version 2016-09-12
 */
@MyBatisDao
public interface ProviderDao extends CrudDao<Provider> {

    List<Provider> findListByName(String name);
}