/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.apimanager.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.thinkgem.jeesite.common.persistence.DataEntity;
import org.hibernate.validator.constraints.Length;

import java.util.Date;

/**
 * 接口Entity
 * @author zxh
 * @version 2016-09-12
 */
public class Api extends DataEntity<Api> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 名称
	private String requestHost;		// 请求主机
	private String requestPath;		// 请求路径
	private String stripRequestPath;		// 玻璃请求路径
	private String upstreamUrl;		// 代理根路径
	private String preserveHost;		// 保持主机
	private Date createdAt;		// 创建时间
	private String providerId;		// 所属服务
	private String providerName; //服务名称
	
	public Api() {
		super();
	}

	public Api(String id){
		super(id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getRequestHost() {
		return requestHost;
	}

	public void setRequestHost(String requestHost) {
		this.requestHost = requestHost;
	}
	
	public String getRequestPath() {
		return requestPath;
	}

	public void setRequestPath(String requestPath) {
		this.requestPath = requestPath;
	}
	
	public String getStripRequestPath() {
		return stripRequestPath;
	}

	public void setStripRequestPath(String stripRequestPath) {
		this.stripRequestPath = stripRequestPath;
	}
	
	public String getUpstreamUrl() {
		return upstreamUrl;
	}

	public void setUpstreamUrl(String upstreamUrl) {
		this.upstreamUrl = upstreamUrl;
	}
	
	public String getPreserveHost() {
		return preserveHost;
	}

	public void setPreserveHost(String preserveHost) {
		this.preserveHost = preserveHost;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	@Length(min=1, max=64, message="所属服务长度必须介于 1 和 64 之间")
	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}
}