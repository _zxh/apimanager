/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.apimanager.web;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.apimanager.entity.Consumer;
import com.thinkgem.jeesite.modules.apimanager.service.ConsumerService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * 接口用户Controller
 * @author zxh
 * @version 2016-09-12
 */
@Controller
@RequestMapping(value = "${adminPath}/apimanager/consumer")
public class ConsumerController extends BaseController {

	@Autowired
	private ConsumerService consumerService;
	
	@ModelAttribute
	public Consumer get(@RequestParam(required=false) String id) {
		Consumer entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = consumerService.get(id);
		}
		if (entity == null){
			entity = new Consumer();
		}
		return entity;
	}
	
	@RequiresPermissions("apimanager:consumer:view")
	@RequestMapping(value = {"list", ""})
	public String list(Consumer consumer, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Consumer> page = consumerService.findPage(new Page<Consumer>(request, response), consumer); 
		model.addAttribute("page", page);
		return "modules/apimanager/consumerList";
	}

	@RequiresPermissions("apimanager:consumer:view")
	@RequestMapping(value = "form")
	public String form(Consumer consumer, Model model) {
		model.addAttribute("consumer", consumer);
		return "modules/apimanager/consumerForm";
	}

	@RequiresPermissions("apimanager:consumer:edit")
	@RequestMapping(value = "save")
	public String save(Consumer consumer, Model model, RedirectAttributes redirectAttributes) {
		if(StringUtils.isEmpty(consumer.getId())){
			consumer.setCreatedAt(new Date());
		}
		if (!beanValidator(model, consumer)){
			return form(consumer, model);
		}
		consumerService.save(consumer);
		addMessage(redirectAttributes, "保存用户成功");
		return "redirect:"+Global.getAdminPath()+"/apimanager/consumer/list?repage";
	}
	
	@RequiresPermissions("apimanager:consumer:edit")
	@RequestMapping(value = "delete")
	public String delete(Consumer consumer, RedirectAttributes redirectAttributes) {
		consumerService.delete(consumer);
		addMessage(redirectAttributes, "删除用户成功");
		return "redirect:"+Global.getAdminPath()+"/apimanager/consumer/list?repage";
	}

}