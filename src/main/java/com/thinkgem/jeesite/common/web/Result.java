/**
 * cn.com.oceansoft.framework.kits.Result
 *
 * @author chenw
 * @create 16/4/27.21:40
 * @email javacspring@hotmail.com
 */

package com.thinkgem.jeesite.common.web;

/**
 * @author chenw
 * @create 16/4/27 21:40
 * @email javacspring@gmail.com
 */
public class Result {

    private boolean succ;
    private int code;
    private String msg;
    private Object data;

    public Result(boolean succ, int code, String msg, Object data) {
        this.succ = succ;
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public Result() {
        this(true, 200, "", null);
    }

    public Result(String msg) {
        this(true, 200, msg, null);
    }

    public static Result succ(int code) {
        return new Result(true, 200, "", null);
    }

    public static Result succ(int code, String msg) {
        return new Result(true, code, msg, null);
    }

    public static Result succ(String msg) {
        return new Result(true, 200, msg, null);
    }

    public static Result succ(String msg, Object data) {
        return new Result(true, 200, msg, data);
    }

    public static Result succ(Object data) {
        return new Result(true, 200, "", data);
    }

    public static Result succ(int code, Object data) {
        return new Result(true, code, "", data);
    }

    public static Result failure(int code) {
        return new Result(false, code, "", null);
    }

    public static Result failure(int code, String msg) {
        return new Result(false, code, msg, null);
    }

    public boolean isSucc() {
        return succ;
    }

    public void setSucc(boolean succ) {
        this.succ = succ;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }


}
