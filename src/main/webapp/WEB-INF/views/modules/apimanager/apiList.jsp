<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>接口管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/apimanager/api/">接口列表</a></li>
		<shiro:hasPermission name="apimanager:api:edit"><li><a href="${ctx}/apimanager/api/form">接口添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="api" action="${ctx}/apimanager/api/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>名称：</label>
				<form:input path="name" htmlEscape="false" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>名称</th>
				<th>请求主机</th>
				<th>请求路径</th>
				<th>玻璃请求路径</th>
				<th>代理根路径</th>
				<th>保持主机</th>
				<shiro:hasPermission name="apimanager:api:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="api">
			<tr>
				<td><a href="${ctx}/apimanager/api/form?id=${api.id}">
					${api.name}
				</a></td>
				<td>
					${api.requestHost}
				</td>
				<td>
					${api.requestPath}
				</td>
				<td>
					${api.stripRequestPath}
				</td>
				<td>
					${api.upstreamUrl}
				</td>
				<td>
					${api.preserveHost}
				</td>
				<shiro:hasPermission name="apimanager:api:edit"><td>
    				<a href="${ctx}/apimanager/api/form?id=${api.id}">修改</a>
					<a href="${ctx}/apimanager/api/delete?id=${api.id}" onclick="return confirmx('确认要删除该接口吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>